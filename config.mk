
# bt1120, xc7022_lrt, usb
videoInput=xc7022_lrt

# v1, v2
commVer=v2

# jtag, uart or ram
PRINT_OUTPUT ?= jtag

# yes, no
H264_ENABLE=no

# MAIN_TSL, MAIN_LIGHTYEAR, MAIN_MWD, MAIN_SSDDEMO
mainFunc=MAIN_TSL

# lightyearBoard, mwdBoard, usbBoard, wsensorBoard
initConfig = wsensorBoard

# freq912MHz, freq733MHz
ddrFreq=freq912MHz

# USE_NEW_BOOTLOADER, USE_OLD_BOOTLOADER
sysBootloader=USE_NEW_BOOTLOADER
