#!/bin/bash
# $1 output elf name
# $2 prod name
# $3 revision 

source findRsVer.sh

export PATH=$PATH:/home/qiuguoliang/work/git_new/tools/00.87.3/linux64/sparc-myriad-rtems-6.3.0/bin
major=0
minor=2
if [ "$3" = "" ] ; then
	revision=`TZ="Asia/Shanghai" date "+%y%m%d%H%M"`
else
    revision=$3
fi
prod=$2
echo "Version:" $major.$minor.$revision
versionFile=leon/version.c
if [ "$1" = "" ] ; then
    outputelf=app.elf
else
    outputelf=$1
fi
echo "outputelf" $outputelf
rs_ver=`find_version`
rs_date=`find_date`
targetelf=boot.$major.$minor.$revision.$rs_ver-$rs_date.$prod.elf
echo "targetelf" $targetelf

echo "#ifndef YK_VERION_H" > $versionFile
echo "#define YK_VERION_H" >> $versionFile

echo "#define VERSION_MAJOR " $major >> $versionFile
echo "#define VERSION_MINOR " $minor >> $versionFile
echo "#define VERSION_REVISION  " $revision >> $versionFile
echo "#endif" >> $versionFile

# force to recompile hostinterface.c
touch leon/hostinterface.cpp

make all -j8

cp output/$outputelf $targetelf
sparc-myriad-rtems-strip $targetelf

echo -e "\033[32m"
echo $(date "+%Y-%m-%d %H:%M:%S") $targetelf "is generated."
echo -e "\033[0m"
