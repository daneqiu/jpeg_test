# 1. 简介
此文档记录fanxing_main分支的代码信息

# 2. 版本分支
## 2.1. 分支
master为繁星的主分支. u棒, 光年, 繁星板都使用此分支

## 2.2. 版本管理约定

## 2.3. 相关工程
### 2.3.1. mdk

### 2.3.2. 主机工具

### 2.3.3. 算法


# 3. 编译
## 3.1. config.mk

## 3.2. 编译步骤


# 4. 代码构架

## 4.1. 驱动
### 4.1.1. 内存分配

### 4.1.2. shave

### 4.1.3. cache

### 4.1.4. vi
参见模块-vi章节

### 4.1.5. emmc

### 4.1.6. usb


## 4.2. 模块
### 4.2.1. vi
#### 4.2.1.1. bt1120

#### 4.2.1.2. xc7022_lrt

#### 4.2.1.3. usb

#### 4.2.1.4. ov5658_lrt

#### 4.2.1.5. ov5658_ipipe
已废弃. movi老isp框架下的5658驱动, 占用很大的cmx资源导致shave无法完全使用.

### 4.2.2. initConfig
#### 4.2.2.1. lightyearBoard

#### 4.2.2.2. mwdBoard
未验证使用

#### 4.2.2.3. usbBoard

#### 4.2.2.4. wsensorBoard


### 4.2.3. comm
#### 4.2.3.1. commv1
已废弃

#### 4.2.3.2. commv2

### 4.2.4. rsH264

### 4.2.5. trackFuncPolicy
```C
typedef struct fx_track_func_policy_coef 
{
    int        policy_sel;					  // policy selection
    union
    {
        float  coef[16];
        struct 
        {
            int defaultPara;            			
            int algorithm_loop;            
            int algorithm_length;          
            float confidence_th;           /*人脸置信度阈值：0-1：默认值-0.25*/
            float facescore_th;            /*人脸正脸值阈值：0-1：默认值-0.5*/
            float blur_th;                 /*人脸模糊度阈值：0-1：默认值-0.5*/
            float pitch_th;                /*人脸角度阈值——pitch方向: 0-90°*/
            float yaw_th;                  /*人脸角度阈值——yaw方向: 0-90°*/
            float roll_th;                 /*人脸角度阈值——roll方向: 0-90°*/
        }   faceTrack;
    } coef;
}__attribute__((aligned(4))) FX_TRACK_FUNC_POLICY_COEF;
```
trackFuncPolicy_createForRsFace的流程图
```mermaid
graph TD
node0[on mode start] --> node1{policy_sel > 0x80}
node1 -- no --> node2[使用rsAlgorithm中的策略]
node1 -- yes --> node3[使用trackFuncPolicy中的策略]
node2 --> node4{defaultPara == 0}
node4 -- yes --> node5[使用FX_TRACK_FUNC_POLICY_COEF.coef.faceTrack<br>来初始化AlgorithmThreshold.face_xx_th]
node4 -- no --> node6[使用固件中的默认AlgorithmThreshold.face_xx_th]

node3 --> node8[trackFuncPolicy使用FX_TRACK_FUNC_POLICY_COEF.coef.faceTrack]

node5 --> endn[模式切换结束]
node6 --> endn
node8 --> endn
```

### 4.2.6. 调试功能
#### 4.2.6.1. temperature

#### 4.2.6.2. imgRecorder

#### 4.2.6.3. cpuusage

#### 4.2.6.4. printToBuf