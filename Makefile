MDK_ROOT = /home/qiuguoliang/work/git_new
GUZZI_REPO =  $(realpath ../../../..)
GUZZI_INC_DIR =  $(realpath $(MDK_ROOT)/mdk/packages/movidius/cdk/arch/$(SOC_REV_DRV)/include)
GUZZI_LIB_DIR =  $(realpath $(MDK_ROOT)/mdk/packages/movidius/cdk/arch/$(SOC_REV_DRV)/lib)

MV_TOOLS_VER=00.87.3
MV_TOOLS_DIR = $(MDK_ROOT)/tools

MV_COMMON_BASE  ?= $(MDK_ROOT)/mdk/common

APP_FOLDER_PATH = $(realpath .)
PRO_FOLDER_PATH = $(realpath ..)
APP_FOLDER_NAME := $(shell basename $(APP_FOLDER_PATH))
PRO_FOLDER_NAME := $(shell basename $(PRO_FOLDER_PATH))

include config.mk

$(info ########################## BUILD settings ##########################)
MV_SOC_PLATFORM  = myriad2
MV_SOC_REV = ma2450
SOC_REV_DRV = ma2x5x
AppConfig = mv182_OV5658_ma2150

######################### algorithm setting ################################
#ifeq ($(MV_SOC_REV),ma2150)
#CCOPT_EXTRA += -DMA2150
#endif
DEMO_VERSION ?= head
################## MvTensor settings #################
MV_FATHOM_RUN_DIR ?= .
MV_TENSOR_BASE ?= $(MDK_ROOT)/mdk/projects/MvTensor

# #ifeq ($(MV_SOC_REV),ma2150)
# LEON_HEADERS += $(wildcard $(MV_FATHOM_RUN_DIR)/rs_body/leon/*.h)
# LEON_COMPONENT_HEADERS_PATHS += $(MV_FATHOM_RUN_DIR)/rs_body/leon
# LEON_COMPONENT_CPP_SOURCES_LOS += $(wildcard $(MV_FATHOM_RUN_DIR)/rs_body/leon/*.cpp)

# ifeq ($(DEMO_VERSION),head)
# LEON_HEADERS += $(wildcard $(MV_FATHOM_RUN_DIR)/rs_head/leon/*.h)
# LEON_COMPONENT_HEADERS_PATHS += $(MV_FATHOM_RUN_DIR)/rs_head/leon
# LEON_COMPONENT_CPP_SOURCES_LOS += $(wildcard $(MV_FATHOM_RUN_DIR)/rs_head/leon/*.cpp)
# else
# CCOPT_EXTRA += -DHANDMODE
# LEON_HEADERS += $(wildcard $(MV_FATHOM_RUN_DIR)/rs_hand/leon/*.h)
# LEON_COMPONENT_HEADERS_PATHS += $(MV_FATHOM_RUN_DIR)/rs_hand/leon
# LEON_COMPONENT_CPP_SOURCES_LOS += $(wildcard $(MV_FATHOM_RUN_DIR)/rs_hand/leon/*.cpp)
# endif
# #endif

#LEON_HEADERS += $(wildcard $(MV_FATHOM_RUN_DIR)/rs_face/leon/*.h)
#LEON_COMPONENT_HEADERS_PATHS += $(MV_FATHOM_RUN_DIR)/rs_face/leon
#LEON_COMPONENT_CPP_SOURCES_LOS += $(wildcard $(MV_FATHOM_RUN_DIR)/rs_face/leon/*.cpp)

#LEON_HEADERS += $(wildcard $(MV_FATHOM_RUN_DIR)/rs_tools/leon/*.h)
#LEON_COMPONENT_HEADERS_PATHS += $(MV_FATHOM_RUN_DIR)/rs_tools/leon
#LEON_COMPONENT_CPP_SOURCES_LOS += $(wildcard $(MV_FATHOM_RUN_DIR)/rs_tools/leon/*.cpp)

#LEON_HEADERS += $(wildcard $(MV_FATHOM_RUN_DIR)/rs_license/leon/*.h)
#LEON_COMPONENT_HEADERS_PATHS += $(MV_FATHOM_RUN_DIR)/rs_license/leon
#LEON_COMPONENT_CPP_SOURCES_LOS += $(wildcard $(MV_FATHOM_RUN_DIR)/rs_license/leon/*.cpp)

LEON_HEADERS += $(wildcard $(MV_FATHOM_RUN_DIR)/leon/*.h)
LEON_COMPONENT_HEADERS_PATHS += $(MV_FATHOM_RUN_DIR)/leon
LEON_COMPONENT_CPP_SOURCES_LOS += $(wildcard $(MV_FATHOM_RUN_DIR)/leon/*.cpp)


################## Appilcation settings #################

CCOPT_EXTRA += -DFATHOMRUN
MVCC_OPT_LEVEL ?= -O3
TARGET_BOARD 	?= MV0212

LEONOPTLEVEL = -O3
MVCCOPT += -ffast-math -fslp-vectorize

################## lhf setting ###########################
#add by lhf
LEON_HEADERS += $(wildcard $(MV_FATHOM_RUN_DIR)/archive/*.h)
LEON_HEADERS += $(wildcard $(MV_FATHOM_RUN_DIR)/archive/include/*.h)
LEON_HEADERS += $(wildcard $(MV_FATHOM_RUN_DIR)/archive/include/*.hpp)
LEON_HEADERS += $(wildcard $(MV_FATHOM_RUN_DIR)/archive/include/cereal/*.hpp)
LEON_HEADERS += $(wildcard $(MV_FATHOM_RUN_DIR)/archive/include/cereal/archives/*.hpp)
LEON_HEADERS += $(wildcard $(MV_FATHOM_RUN_DIR)/archive/include/cereal/details/*.hpp)
LEON_HEADERS += $(wildcard $(MV_FATHOM_RUN_DIR)/archive/include/cereal/external/*.hpp)
LEON_HEADERS += $(wildcard $(MV_FATHOM_RUN_DIR)/archive/include/cereal/external/rapidjison/*.hpp)
LEON_HEADERS += $(wildcard $(MV_FATHOM_RUN_DIR)/archive/include/cereal/external/rapidxml/*.hpp)
LEON_HEADERS += $(wildcard $(MV_FATHOM_RUN_DIR)/archive/include/cereal/types/*.hpp)
LEON_HEADERS += $(wildcard $(MV_FATHOM_RUN_DIR)/archive/include/cereal/concepts/*.hpp)
LEON_COMPONENT_HEADERS_PATHS += $(MV_FATHOM_RUN_DIR)/archive
LEON_COMPONENT_CPP_SOURCES_LOS += $(wildcard $(MV_FATHOM_RUN_DIR)/archive/*.cpp)
LEON_COMPONENT_CPP_SOURCES_LOS += $(wildcard $(MV_FATHOM_RUN_DIR)/archive/src/*.cpp)
 
DirLDScrCommon = $(MV_COMMON_BASE)/scripts/ld/$(MV_SOC_PLATFORM)memorySections
DdrInitHeaderMvcmd   ?= $(MV_COMMON_BASE)/utils/ddrInit/ddrInitHeaderForMvcmd_$(MV_SOC_REV)
#MVCONVOPT ?= -cv:$(MV_SOC_REV)
MvCmdfile = ./output/$(APPNAME)_ddrinit.mvcmd
DefaultFlashScript = ./scripts/flash/default_flash_$(MV_SOC_PLATFORM).scr


##### USB setting #####
OUTPUT_UNIT 	?= USB

BOARD_SUPPORT_MIPI = yes
BOARD_SUPPORT_HDMI = yes
BOARD_SUPPORT_USB  = yes

ENABLE_JPEG_ENCODER = yes

CCOPT += -DOUTPUT_UNIT_IS_USB
CCOPT += -DINPUT_UNIT_IS_USB
CCOPT += -D'USB_REFCLK_MHZ=USB_REFCLK_20MHZ'

RTEMS_USB_LIB_BUILD_TYPE = release
MV_USB_PROTOS = protovsc2
#MV_USB_PROTOS = protovideo

MV_LOCAL_OBJ=yes
MV_SOC_OS = rtems
RTEMS_BUILD_NAME =b-prebuilt
CCOPT_EXTRA += -D'USE_SOFTWARE=1'
DISABLE_ERROR_ON_WARNINGS ?=yes
# CCOPT += -D'DEFAULT_APP_CLOCK_KHZ=600000'

# select yes to use JTAG or select no to use UART for debug logs
JTAG_LOG        ?= yes

CCOPT += -D$(TARGET_BOARD)
CCOPT += -D'CRITICAL_SECTION_ENABLE=1'
CCOPT += -DNO_PRINT
CCOPT += -DAPP_CONFIGURATION -g
CCOPT += -D'MAX_NR_OF_CAMS                   = 1'
CCOPT			+= -DDEBUG
#CCOPT += -DNO_PRINT
CCOPT +=-DDEFAULT_ALLOC_TIMEOUT_MS=4000

CCOPT += -D__MMS_DEBUG__
CCOPT += -D___RTEMS___
CCOPT += -DSPI_SLAVE
DefaultSparcRTEMSLibs += $(GUZZI_LIB_DIR)/libguzzi_rtems_$(AppConfig).a
DefaultSparcRTEMSLibs += $(GUZZI_LIB_DIR)/libversion_info.a
#add by lhf 20180123
CCOPT += -I$(MDK_ROOT)/mdk/common/components/MV0212/leon/bm/include
CCOPT += -I$(MDK_ROOT)/mdk/common/components/MV0212/leon/common/include

############################################################
ComponentList_LOS += UnitTestVcs \
					VcsHooks    \
					I2CSlave    \
					Fp16Convert	\
					USB_VSC		\
					MV0212
CCOPT += -D'$(mainFunc)=1'
CCOPT += -D'$(sysBootloader)=1'


############################
TEST_WATCHPOINT = no
# WatchPoint components for debug
ifeq ($(TEST_WATCHPOINT), yes)
$(warning Warning: Test Watchpoint components)
CCOPT += -DTESTWATCHPOINT
CCOPT_LRT += -DTESTWATCHPOINT
MVCCOPT += -DTESTWATCHPOINT
ComponentList += WatchPoint
endif

########################## SELECT INIT CONFIG ##############
# bt1120 , ov5656_ipipe, ov5656_lrt, xc7022_lrt, usb
RS_INITCONFIG_COMPONENTS=../../projects/$(APP_FOLDER_NAME)/leon/initConfig
ComponentList_LOS += $(RS_INITCONFIG_COMPONENTS)/$(initConfig)

ifeq ($(ddrFreq), freq733MHz)
# 733MHz
DdrInitElf  =  $(MV_COMMON_BASE)/utils/ddrInit/ddrInit_ma2450_733Mhz.elf
else
# 912MHz
DdrInitElf  =  $(MV_COMMON_BASE)/utils/ddrInit/ddrInit_ma2450.elf
endif

########################## END INIT CONFIG  ################

ifeq ($(ENABLE_JPEG_ENCODER), yes)
IPIPE_COMPONENTS = ../../examples/cdk/components/arch/ma2x5x
ComponentList_LOS   += $(IPIPE_COMPONENTS)/JpegM2EncoderDyn
#ComponentList_LOS += JpegM2EncoderDyn
ComponentList_SVE   := $(IPIPE_COMPONENTS)/JpegM2EncoderDyn
#ComponentList_SVE += JpegM2EncoderDyn
ComponentList_SVE   += kernelLib/MvCV/kernels/dct16bit kernelLib/MvCV/kernels/jpegGetBlockY420 kernelLib/MvCV/kernels/huffman_encode
SHAVE_COMPONENTS=yes
CCOPT += -I$(MDK_ROOT)/mdk/examples/cdk/components/arch/ma2x5x/JpegM2EncoderDyn
CCOPT += -I$(MV_COMMON_BASE)/components/flic/common/commonTypes/leon
endif
ComponentList_SVE = kernelLib/MvCV/include
ifeq ($(ENABLE_JPEG_ENCODER), yes)
CCOPT += -D'ENABLE_JPEG'
CCOPT += -D'JPEG_FIRST_SHV=8'
CCOPT += -D'JPEG_USED_SHV=1'
endif





ifeq ($(ENABLE_JPEG_ENCODER), yes)
LinkerScript = config/custom_jpeg.xc7022_lrt.ldscript
else
LinkerScript = config/custom.xc7022_lrt.ldscript
endif


########################## END VI ##########################
# PRINT_OUTPUT = jtag, uart or ram
PRINT_OUTPUT ?= jtag
# PRINT_OUTPUT ?= uart
ifeq ($(PRINT_OUTPUT),)
$(error ERROR: PRINT_OUTPUT is not defined. Please define it in config.mk)
endif
ifeq ($(PRINT_OUTPUT), jtag)
ComponentList += PipePrint
ComponentList_LRT += PipePrint
CCOPT += -D'REDIRECT_PRINTF_TO_RAM=0'
else ifeq ($(PRINT_OUTPUT),ram)
CCOPT += -D'REDIRECT_PRINTF_TO_RAM=1'
endif

# Ensure that the we are using the correct rtems libs etc RTEMS ADDING
#########################################################################################

#########################################################################################
# OS-Leon (SPI slave api)
#########################################################################################
SPI_SLAVE_API_BASE  = $(MV_COMMON_BASE)/components/MessageProtocol/leon/rtems/
CCOPT              += -I$(SPI_SLAVE_API_BASE)/include
LEON_APP_C_SOURCES += $(SPI_SLAVE_API_BASE)/src/MessRingBuff.c
LEON_APP_C_SOURCES += $(SPI_SLAVE_API_BASE)/src/MessageProtocol.c
LEON_APP_C_SOURCES += $(SPI_SLAVE_API_BASE)/src/OsDrvSpiSlaveCP.c
LEON_APP_C_SOURCES += $(SPI_SLAVE_API_BASE)/src/OsMessageProtocol.c

#########################################################################################
#Critical to allow multiple symbols definitions

#########################################################################################
# Include the top makefile
include $(MV_TENSOR_BASE)/mvTensorSources.mk
include $(MV_COMMON_BASE)/generic.mk
include $(MV_TENSOR_BASE)/mvTensorRules.mk

######### Extra app related options ##########

$(info TARGET_BOARD: $(TARGET_BOARD))
$(info OUTPUT_UNIT:$(OUTPUT_UNIT))
$(info LD script: $(DirLDScrCommon))
$(info Flash script: $(DefaultFlashScript))
$(info VideoInput: $(videoInput))
$(info Comm version: $(commVer))
$(info ComponentList_LOS: $(ComponentList_LOS))
$(info ComponentList_LRT: $(ComponentList_LRT))
$(info LinkerScript: $(LinkerScript))
$(info mainFunc: $(mainFunc))
$(info LDOPT: $(LDOPT))

#########################################################################################
# Boot over spi (with DDR init) rule

output/$(APPNAME)_ddrinit.mvcmd: spi-boot

spi-boot:  output/$(APPNAME).mvcmd
	make output/$(APPNAME).mvcmd -j CCOPT_EXTRA="-DNO_PRINT" CCOPT_EXTRA_LRT="-DNO_PRINT"

	dd if=/dev/zero of=output/nulllcmd.mvcmd bs=1K count=1 # at 10MHz boot we should have a 819us timeout which is almost double thant the required 450us for DDr init reported
	cat $(DdrInitHeaderMvcmd) \
		./output/nulllcmd.mvcmd \
		./output/$(APPNAME).mvcmd > ./output/$(APPNAME)_ddrinit.mvcmd

TEST_TYPE := "MANUAL"
$(info ####################################################################)