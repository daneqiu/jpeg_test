#!/bin/bash

find_version()
{
    a=`cat rs_api/leon/rs_api.h  | grep "#define ALGORITHM_VERSION"`
    for word in $a; 
    do 
    :
    done
    tmp=${word:1}
    echo ${tmp%\"*}
}

find_date()
{
    a=`cat rs_api/leon/rs_api.h  | grep "#define ALGORITHM_DATE"`
    for word in $a; 
    do 
    :
    done
    tmp=${word:1}
    datestr=${tmp%\"*}
    b=${datestr////.}
    echo $b
}