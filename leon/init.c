#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include <errno.h>
#include <rtems.h>
#include <bsp.h>

#include <semaphore.h>
#include <pthread.h>
#include <sched.h>
#include <fcntl.h>
#include <mv_types.h>
#include <rtems/cpuuse.h>
#include <DrvLeon.h>
#include <initSystem.h>
#include <rtems/bspIo.h>

int main(int argc, char **argv);

void* POSIX_Init (void *args)
{
    UNUSED(args);

    main(0, NULL);
	return NULL;
}

#if !defined (__CONFIG__)
#define __CONFIG__

/* ask the system to generate a configuration table */
#define CONFIGURE_INIT

#define CONFIGURE_MICROSECONDS_PER_TICK 1000 /* 1 milliseconds */

#define CONFIGURE_TICKS_PER_TIMESLICE 10 

#define CONFIGURE_APPLICATION_NEEDS_CONSOLE_DRIVER

#define CONFIGURE_APPLICATION_NEEDS_CLOCK_DRIVER

#define CONFIGURE_POSIX_INIT_THREAD_TABLE

#define  CONFIGURE_MINIMUM_TASK_STACK_SIZE      (8192*4)

#define  CONFIGURE_STACK_CHECKER_ENABLED

#define CONFIGURE_MAXIMUM_TASKS                 20

#define CONFIGURE_MAXIMUM_POSIX_THREADS              50

#define CONFIGURE_MAXIMUM_POSIX_MUTEXES              600

#define CONFIGURE_MAXIMUM_POSIX_KEYS                 50

#define CONFIGURE_MAXIMUM_POSIX_SEMAPHORES           600

#define CONFIGURE_MAXIMUM_POSIX_CONDITION_VARIABLES 16

#define CONFIGURE_MAXIMUM_POSIX_TIMERS          4

#define CONFIGURE_MAXIMUM_TIMERS                4

#define CONFIGURE_MAXIMUM_MESSAGE_QUEUES 8
#define CONFIGURE_MAXIMUM_TASKS                 4

#define CONFIGURE_MAXIMUM_DRIVERS 10
#define CONFIGURE_MAXIMUM_SEMAPHORES 16
#define CONFIGURE_MAXIMUM_DEVICES 8
#define CONFIGURE_APPLICATION_NEEDS_ZERO_DRIVER
#define CONFIGURE_APPLICATION_NEEDS_NULL_DRIVER
#define CONFIGURE_APPLICATION_NEEDS_STUB_DRIVER

#define CONFIGURE_USE_IMFS_AS_BASE_FILESYSTEM

#define CONFIGURE_FILESYSTEM_DOSFS

#define CONFIGURE_MAXIMUM_DRIVERS 10

// Add dynamic semaphore allocation 
#define CONFIGURE_MAXIMUM_SEMAPHORES rtems_resource_unlimited(5) 

#define CONFIGURE_LIBIO_MAXIMUM_FILE_DESCRIPTORS 30

#define CONFIGURE_APPLICATION_NEEDS_LIBBLOCK

#define CONFIGURE_POSIX_INIT_TASKS_TABLE

#define CONFIGURE_BDBUF_MAX_READ_AHEAD_BLOCKS  	(16)

#define CONFIGURE_BDBUF_MAX_WRITE_BLOCKS       	(64)

#define CONFIGURE_BDBUF_BUFFER_MIN_SIZE 		(512)

#define CONFIGURE_BDBUF_BUFFER_MAX_SIZE 		(32 * 1024)

#define CONFIGURE_BDBUF_CACHE_MEMORY_SIZE 		(4 * 1024 * 1024)

#define CONFIGURE_MAXIMUM_USER_EXTENSIONS    1

extern void Fatal_extension(
        Internal_errors_Source  the_source,
        bool                    is_internal,
        uint32_t                the_error
);

#define CONFIGURE_INITIAL_EXTENSIONS         { .fatal = Fatal_extension }

#include <rtems/confdefs.h>

#endif /* if defined CONFIG */
