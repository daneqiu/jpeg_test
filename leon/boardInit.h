#define BOARD_INIT_H__
#if 1

#include "OsDrvSpiBus.h"

#define SPI_BUS_NAME     "/dev/spi"
#define SPI_FLASH_NAME   "flash"
#define FLASH_DEVICE     "/dev/spi.flash"

#define MNT_DIR          "/mountedRamDisk"
#define MAIN_APP_NAME    "boot.elf"
#define BACKUP_APP_NAME  "backup.elf"

// Magic numbers to identify if an image is present
#define VALID_HEADER_MAGIC   (0xABCD1234)

//Global APP id
#define ID_NORMAL_APP       (0)
#define ID_BACKUP_APP       (1)
#define ID_NORMAL_MODEL     (2)




// Maximum number of applications that can be stored
#define MAX_APPLIACIONS_IN_FLASH  (2)
#define LAUNCHER_RESERVED_SIZE    (1024 * 1024) // 1M

#define LICENSE_DATA_OFFSET ((1024 * 1024 * 7) + 512)

// Flash chip specific defines
#define FLASH_CHIP_SIZE           (8 * 1024 * 1024)//8M
#define SUBSECTOR_SIZE            (4096)

// Macro to align sizes and addresses to the flash subsector size
#define ALIGN_TO_SUBSECTOR(S)  ((((S) + (SUBSECTOR_SIZE) - 1)) & ~((SUBSECTOR_SIZE) - 1))

#define ALGORITHM_LICENSE_SIZE (128) //12KB

// Information about each of the application images


// Start of the boot context in flash


void myr2_spi_test_pin_config(void);
int myr2_register_libi2c_spi_bus(void);

int initUart(int baudrate);
#endif
