/******************************************************************************

 @File         : initSystem.c
 @Author       : MT
 @Brief        : IPIPE Hw configuration on Los side
 Date          : 02 - Sep - 2014
 E-mail        : xxx.xx@movidius.com
 Copyright     : Movidius Srl 2013, Movidius Ltd 2014

 Description :


******************************************************************************/


/**************************************************************************************************
 ~~~ Included types first then APIs from other modules
 **************************************************************************************************/
#include <OsDrvCpr.h>
#include <DrvGpio.h>
#include "DrvCpr.h"
#include <DrvDdr.h>
#include <OsDrvTimer.h>
#include "DrvTimer.h"
#include "swcLeonUtils.h"
#include <OsDrvSvu.h>
#include <OsDrvCmxDma.h>
#include <OsDrvShaveL2Cache.h>
#include <DrvShaveL2Cache.h>
#include "initSystem.h"
#include "brdGpioCfgs/brdMv0182GpioDefaults.h"
#include "boardInit.h"
#include <myriad2_version.h>
#ifdef TESTWATCHPOINT
#include <WatchPoint.h>
#endif
#include "emmc.h"
//#include "wdtAndFatalExtension.h"
//#include "temperature.h"
#include "initConfig.h"

/**************************************************************************************************
 ~~~  Specific #defines and types (typedef,enum,struct)
**************************************************************************************************/
#ifdef ENABLE_JPEG

#ifndef JPEG_FIRST_SHV
#define JPEG_FIRST_SHV  (8)
#endif

#ifndef JPEG_USED_SHV
#define JPEG_USED_SHV   (2)
#endif

#ifndef JPEG_L2_DATA_CACHE_PARTITION
#define JPEG_L2_DATA_CACHE_PARTITION (0)
#endif

#ifndef JPEG_L2_INSTR_CACHE_PARTITION
#define JPEG_L2_INSTR_CACHE_PARTITION (0)
#endif

#endif


#define SHAVES_USED             12
#define CMX_CONFIG_SLICE_7_0       (0x11111111)
#define CMX_CONFIG_SLICE_15_8      (0x11111111)
#define L2CACHE_CFG                (SHAVE_L2CACHE_NORMAL_MODE)
// #define L2CACHE_CFG                (SHAVE_L2CACHE_BYPASS_MODE)

/**************************************************************************************************
 ~~~  Global Data (Only if absolutely necessary)
**************************************************************************************************/
// u32 __l2_config   __attribute__((section(".l2.mode")))  = L2CACHE_CFG;
CmxRamLayoutCfgType __attribute__((section(".cmx.ctrl"))) __cmx_config = {CMX_CONFIG_SLICE_7_0, CMX_CONFIG_SLICE_15_8};

BSP_SET_L2C_CONFIG(1, L2C_REPL_LRU, 0, L2C_MODE_WRITE_THROUGH, 0, NULL);
// BSP_SET_L2C_CONFIG(0, L2C_REPL_LRU, 0, L2C_MODE_WRITE_THROUGH, 0, NULL);

rtems_id fathomSemId;
/*************************************************************************************************
 ~~~  Static Local Data
 **************************************************************************************************/

static tySocClockConfig clocksConfig =
{
    .refClk0InputKhz         = SYS_CLK_KHZ      /* input oscillator source at 12 MHz */,
    .refClk1InputKhz         = 0,
    .targetPll0FreqKhz       = DEFAULT_APP_CLOCK_KHZ, //360000,          /* system clock at 360 Mhz */
    .targetPll1FreqKhz       = 0,               /* will be set by DDR driver */
    .clkSrcPll1              = CLK_SRC_REFCLK0, /* refClk1 is also not enabled for now */
    .masterClkDivNumerator   = 1,
    .masterClkDivDenominator = 1,
    .cssDssClockEnableMask   = CSS_CLOCKS, 
    .mssClockEnableMask      = MSS_CLOCKS,      /* MSS clocks enabled */
    .sippClockEnableMask     = SIPP_CLOCKS,     /* SIPP clocks enabled */
    .upaClockEnableMask      = UPA_CLOCKS,      /* UPA clocks disabled */
    .pAuxClkCfg              = auxClkAllOn,
};

//#############################################################################
#include <DrvLeonL2C.h>
void leonL2CacheInitWrThrough()
{
    LL2CConfig_t ll2Config;

    // Invalidate entire L2Cache before enabling it
    DrvLL2CFlushOpOnAllLines(LL2C_OPERATION_INVALIDATE, /*disable cache?:*/ 0);

    ll2Config.LL2CEnable = 1;
    ll2Config.LL2CLockedWaysNo = 0;
    ll2Config.LL2CWayToReplace = 0;
    ll2Config.busUsage = BUS_WRAPPING_MODE;
    ll2Config.hitRate = HIT_WRAPPING_MODE;
    ll2Config.replacePolicy = LRU;
    ll2Config.writePolicy = WRITE_THROUGH;
    // ll2Config.writePolicy = COPY_BACK;

    DrvLL2CInitialize(&ll2Config);
}

/**************************************************************************************************
 ~~~ Shave Init
**************************************************************************************************/

#define SL2C_DATA_PART  (1 << 0)
#define SL2C_INSTR_PART (1 << 1)

typedef struct ShaveL2CfgEntry
{
    drvShaveL2CachePartitionSizes_enum_t size;
    u8 firstShave;
    u8 lastShave;
    u8 typeMask; // data and/or instruction partition
} ShaveL2CfgEntry_t;

// Partitions are created in consecutive order.
// These IDs must match with the ones used in flush/invalidate calls
ShaveL2CfgEntry_t sl2cArray[] =
{
    /* 0 */ { SHAVEPART128KB,  8,  11, SL2C_DATA_PART}, // H264 - hardcoded in component, FIXME
    /* 1 */ {  SHAVEPART64KB,  0,  7, SL2C_DATA_PART}, // MvTensor Data
    /* 2 */ {  SHAVEPART64KB,  0,  7, SL2C_INSTR_PART}, // MvTensor Ins

    ///* 3 */ { SHAVEPART64KB,  8,  11, SL2C_INSTR_PART}, // JPEG

    /*--*/ {              0,  0,  0,              0}, // Array terminator: type=0
};


int shaveL2CacheInit(ShaveL2CfgEntry_t *cfg)
{
    int sc;
    // Set the shave L2 Cache mode
    sc = OsDrvShaveL2CacheInit(L2CACHE_CFG);
    if(sc)
       return sc;

    // Get Shave L2 cache partitions
    for(int part = 0; cfg[part].typeMask != 0; part++) {
        int partition_id;
        sc = OsDrvShaveL2CGetPartition(cfg[part].size, &partition_id);
        if(sc)
           return sc;
        // Returned partition number must match with our assignment,
        // hardcoded with defines in Makefile
        assert(part == partition_id);
    }

    // Allocate Shave L2 cache set partitions
    sc = OsDrvShaveL2CacheAllocateSetPartitions();
    if(sc)
        return sc;

    // Assign the partitions to shaves and invalidate them
    for(int part = 0; cfg[part].typeMask != 0; part++) {
        for (int shv = cfg[part].firstShave; shv <= cfg[part].lastShave; shv++) {
            if (cfg[part].typeMask & SL2C_DATA_PART) {
                sc = OsDrvShaveL2CSetNonWindowedPartition(shv, part,
                        NON_WINDOWED_DATA_PARTITION);
                if(sc)
                   return sc;
			   sc = OsDrvShaveL2CSetWindowPartition(shv, SHAVEL2CACHEWIN_C, part);
            }
            if (cfg[part].typeMask & SL2C_INSTR_PART) {
                sc = OsDrvShaveL2CSetNonWindowedPartition(shv, part,
                        NON_WINDOWED_INSTRUCTIONS_PARTITION);
                if(sc)
                   return sc;
            }
        }

        sc = OsDrvShaveL2CachePartitionInvalidate(part);
        if(sc)
            return sc;
    }

    return 0;
}

/**************************************************************************************************
 ~~~ Functions Implementation
**************************************************************************************************/
int initClocksAndMemory(void)
{
//    int retVal;
    // Configure the system
    OsDrvCprInit();
    OsDrvCprOpen();
    OsDrvTimerInit();
    // OsDrvCprAuxClockArrayConfig(auxClkAllOn);
    // haan: comment DrvLL2CInitialize, otherwise app boot from flash will be crash in DrvLL2CInitialize
    //leonL2CacheInitWrThrough();
    // Set the shave L2 Cache mode
    //DrvShaveL2CacheSetMode(L2CACHE_CFG);

    // Enable media subsystem gated clocks and resets
    SET_REG_WORD(MSS_CLK_CTRL_ADR,      0xffffffff);
    SET_REG_WORD(MSS_RSTN_CTRL_ADR,     0xffffffff);

    DrvCprSysDeviceAction(MSS_DOMAIN, ASSERT_RESET,  DEV_MSS_LCD |  DEV_MSS_CIF0 | DEV_MSS_CIF1 | DEV_MSS_SIPP | DEV_MSS_AMC);
    DrvCprSysDeviceAction(MSS_DOMAIN, DEASSERT_RESET, -1);
    DrvCprSysDeviceAction(CSS_DOMAIN, DEASSERT_RESET, -1);
    DrvCprSysDeviceAction(SIPP_DOMAIN, DEASSERT_RESET, -1);
    DrvCprSysDeviceAction(UPA_DOMAIN, DEASSERT_RESET, -1);

    s32 sc = OsDrvCprSetupClocks(&clocksConfig);
    if(sc)
        return sc;

    //DrvCprStartAllClocks();

    // Enable PMB subsystem gated clocks and resets
    SET_REG_WORD   (CMX_RSTN_CTRL,0x00000000);               // engage reset
    SET_REG_WORD   (CMX_CLK_CTRL, 0xffffffff);               // turn on clocks
    SET_REG_WORD   (CMX_RSTN_CTRL,0xffffffff);               // dis-engage reset

    // SET_REG_WORD(MSS_SIPP_RSTN_CTRL_ADR, 0);
    // SET_REG_WORD(MSS_SIPP_RSTN_CTRL_ADR, 0x3ffffff);
    // SET_REG_WORD(MSS_SIPP_CLK_SET_ADR,   0x3ffffff);

    int ret_code = 0;
    if ((ret_code = (int)OsDrvCmxDmaInitDefault()) != OS_MYR_DRV_SUCCESS)
    {
        printf("Error: OsDrvCmxDmaInitDefault() FAILED, with error code: %d\n", ret_code);
        return ret_code;
    }
    DrvDdrInitialise(NULL);
    if ((ret_code = (int)rtems_semaphore_create(
        rtems_build_name('F', 'A', 'T', 'R'), 1, RTEMS_DEFAULT_ATTRIBUTES,
        RTEMS_NO_PRIORITY, &fathomSemId)) != RTEMS_SUCCESSFUL)
    {
        printf("Error: rtems_semaphore_create FAILED, with error code: %d\n,", ret_code);
        return ret_code;
    }
    if ((ret_code = (int)OsDrvSvuInit()) != OS_MYR_DRV_SUCCESS)
    {
        printf("Error: OsDrvSvuInit() FAILED, with error code: %d\n", ret_code);
        return ret_code;
    }

    // for (int i = POWER_ISLAND_SHAVE_0; i < POWER_ISLAND_SHAVE_0 + SHAVES_USED; i++)
    //     OsDrvCprPowerTurnOffIsland((enum PowerIslandIndex)i);
#if 1
    shaveL2CacheInit(sl2cArray);
#else
#if 0	
    sc = OsDrvShaveL2CacheInit(L2CACHE_CFG);
    if(sc)
        return sc;
    ret_code = OsDrvShaveL2CGetPartition(SHAVEPART128KB, &part1Id);
    assert(ret_code == OS_MYR_DRV_SUCCESS);
    ret_code = OsDrvShaveL2CGetPartition(SHAVEPART128KB, &part2Id);
    assert(ret_code == OS_MYR_DRV_SUCCESS);
    ret_code = OsDrvShaveL2CacheAllocateSetPartitions();
    assert(ret_code == OS_MYR_DRV_SUCCESS);
    ret_code = OsDrvShaveL2CachePartitionInvalidate(part1Id);
    assert(ret_code == OS_MYR_DRV_SUCCESS);
    ret_code = OsDrvShaveL2CachePartitionInvalidate(part2Id);
    assert(ret_code == OS_MYR_DRV_SUCCESS);
#else
     // Set the shave L2 Cache mode
      OsDrvShaveL2CacheInit(L2CACHE_CFG);
      
      OsDrvShaveL2CResetPartitions();
      //Set Shave L2 cache partitions
      int partition_id;
      OsDrvShaveL2CGetPartition(SHAVEPART256KB, &partition_id);

      //Allocate Shave L2 cache set partitions
      OsDrvShaveL2CacheAllocateSetPartitions();

      //Assign the one partition defined to all shaves
      for (int i = 0; i < 12; i++)
      {
        OsDrvShaveL2CSetNonWindowedPartition(i, 0, NON_WINDOWED_DATA_PARTITION);
        OsDrvShaveL2CSetNonWindowedPartition(i, 0, NON_WINDOWED_INSTRUCTIONS_PARTITION);
      }

      //Make sure the partition is invalidated to begin with to avoid bogus flushes with code corruption
      OsDrvShaveL2CachePartitionInvalidate(0);
#endif
#endif
/*#ifdef ENABLE_JPEG
    int part1Id, part2Id, part3Id, part4Id, part5Id, part6Id;
    ret_code = OsDrvShaveL2CGetPartition(SHAVEPART32KB, &part1Id);
    assert(ret_code == OS_MYR_DRV_SUCCESS);
    ret_code = OsDrvShaveL2CGetPartition(SHAVEPART32KB, &part2Id);
    assert(ret_code == OS_MYR_DRV_SUCCESS);
    ret_code = OsDrvShaveL2CGetPartition(SHAVEPART32KB, &part3Id);
    assert(ret_code == OS_MYR_DRV_SUCCESS);
    ret_code = OsDrvShaveL2CGetPartition(SHAVEPART32KB, &part4Id);
    assert(ret_code == OS_MYR_DRV_SUCCESS);
    ret_code = OsDrvShaveL2CGetPartition(SHAVEPART32KB, &part5Id);
    assert(ret_code == OS_MYR_DRV_SUCCESS);
    ret_code = OsDrvShaveL2CGetPartition(SHAVEPART32KB, &part6Id);
    assert(ret_code == OS_MYR_DRV_SUCCESS);

    printf("%d %d %d %d %d %d\n", part1Id, part2Id, part3Id, part4Id, part5Id, part6Id);
#endif
#ifdef ENABLE_JPEG
    ret_code = OsDrvShaveL2CachePartitionInvalidate(part1Id);
    assert(ret_code == OS_MYR_DRV_SUCCESS);
    ret_code = OsDrvShaveL2CachePartitionInvalidate(part2Id);
    assert(ret_code == OS_MYR_DRV_SUCCESS);
    ret_code = OsDrvShaveL2CachePartitionInvalidate(part3Id);
    assert(ret_code == OS_MYR_DRV_SUCCESS);
    ret_code = OsDrvShaveL2CachePartitionInvalidate(part4Id);
    assert(ret_code == OS_MYR_DRV_SUCCESS);
    ret_code = OsDrvShaveL2CachePartitionInvalidate(part5Id);
    assert(ret_code == OS_MYR_DRV_SUCCESS);
    ret_code = OsDrvShaveL2CachePartitionInvalidate(part6Id);
    assert(ret_code == OS_MYR_DRV_SUCCESS);
#endif


#ifdef ENABLE_JPEG
    for(int i = POWER_ISLAND_SHAVE_0 + JPEG_FIRST_SHV; i < POWER_ISLAND_SHAVE_0 + JPEG_FIRST_SHV + JPEG_USED_SHV; i++)
    {
        OsDrvCprPowerTurnOnIsland((enum PowerIslandIndex)i);
        ret_code = OsDrvShaveL2CSetNonWindowedPartition((shaveId_t)(i-POWER_ISLAND_SHAVE_0), JPEG_L2_DATA_CACHE_PARTITION, NON_WINDOWED_DATA_PARTITION);
        assert(ret_code == OS_MYR_DRV_SUCCESS);
        ret_code = OsDrvShaveL2CSetNonWindowedPartition((shaveId_t)(i-POWER_ISLAND_SHAVE_0), JPEG_L2_INSTR_CACHE_PARTITION, NON_WINDOWED_INSTRUCTIONS_PARTITION);
        assert(ret_code == OS_MYR_DRV_SUCCESS);
        ret_code = OsDrvShaveL2CSetWindowPartition((shaveId_t)(i-POWER_ISLAND_SHAVE_0), SHAVEL2CACHEWIN_C, JPEG_L2_DATA_CACHE_PARTITION);
        assert(ret_code == OS_MYR_DRV_SUCCESS);
    }
#endif*/

#ifdef ENABLE_JPEG
    for(int i = POWER_ISLAND_SHAVE_0 + JPEG_FIRST_SHV; i < POWER_ISLAND_SHAVE_0 + JPEG_FIRST_SHV + JPEG_USED_SHV; i++)
    {
        OsDrvCprPowerTurnOnIsland((enum PowerIslandIndex)i);
        ret_code = OsDrvShaveL2CSetNonWindowedPartition((shaveId_t)(i-POWER_ISLAND_SHAVE_0), JPEG_L2_DATA_CACHE_PARTITION, NON_WINDOWED_DATA_PARTITION);
        assert(ret_code == OS_MYR_DRV_SUCCESS);
        ret_code = OsDrvShaveL2CSetNonWindowedPartition((shaveId_t)(i-POWER_ISLAND_SHAVE_0), JPEG_L2_INSTR_CACHE_PARTITION, NON_WINDOWED_INSTRUCTIONS_PARTITION);
        assert(ret_code == OS_MYR_DRV_SUCCESS);
        ret_code = OsDrvShaveL2CSetWindowPartition((shaveId_t)(i-POWER_ISLAND_SHAVE_0), SHAVEL2CACHEWIN_C, JPEG_L2_DATA_CACHE_PARTITION);
        assert(ret_code == OS_MYR_DRV_SUCCESS);
    }
#endif
    return 0;
}

#if H264_ENCODE
extern char H264OsalX_LibBuildTag;
#endif

void initSystem(void)
{
    initClocksAndMemory();

    
#if H264_ENCODE
    // print 264 libraries for debuging 05-25-2018
    printf("Lib version is %s\n", &H264OsalX_LibBuildTag);
#endif

    DrvGpioIrqResetAll();
    DrvGpioInitialiseRange(brdMV0182GpioCfgDefault);

   // initTemperature();
    // initWdt();

#ifdef TESTWATCHPOINT
    // Disable Breakpoint on Watchpoint and Error on both LEONs
    // For some reasons watchpoints are treated as errors
    //CLR_REG_BITS_MASK(DSU_LEON_OS_CTRL_ADR, 1<<2 | 1<<5);
    //CLR_REG_BITS_MASK(DSU_LEON_RT_CTRL_ADR, 1<<2 | 1<<5);

    LeonAddHWWatchPoint(0, 0x80301A00, 1024, 0, 0, 1);
    AllShaveDataAccessRangeWP(0x80301A00, 1024);
#endif

    // init flash spi   
    myr2_spi_test_pin_config();

    int ret_val;
    if((ret_val = myr2_register_libi2c_spi_bus()) != 0)
    {
        printf("Failed to initialize bus or devices . Fatal ERROR: %d !!! \n", ret_val);
    }

#if 1
    // init uart
    ret_val = initUart(115200);
    if (ret_val)
    {
        printf("Failed to initialize uart. Fatal ERROR: %d !!! \n", ret_val);
    }
#endif

    setup_emmc(0);
}

