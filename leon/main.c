/* =============================================================================
* Copyright (c) 2013-2014 MM Solutions AD
* All rights reserved. Property of MM Solutions AD.
*
* This source code may not be used against the terms and conditions stipulated
* in the licensing agreement under which it has been supplied, or without the
* written permission of MM Solutions. Rights to use, copy, modify, and
* distribute or disclose this source code and its documentation are granted only
* through signed licensing agreement, provided that this copyright notice
* appears in all copies, modifications, and distributions and subject to the
* following conditions:
* THIS SOURCE CODE AND ACCOMPANYING DOCUMENTATION, IS PROVIDED AS IS, WITHOUT
* WARRANTY OF ANY KIND, EXPRESS OR IMPLIED. MM SOLUTIONS SPECIFICALLY DISCLAIMS
* ANY WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
* MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE AND NON-INFRINGEMENT. IN
* NO EVENT SHALL MM SOLUTIONS BE LIABLE TO ANY PARTY FOR ANY CLAIM, DIRECT,
* INDIRECT, SPECIAL, INCIDENTAL, OR CONSEQUENTIAL DAMAGES, INCLUDING LOST
* PROFITS, OR OTHER LIABILITY, ARISING OUT OF THE USE OF OR IN CONNECTION WITH
* THIS SOURCE CODE AND ITS DOCUMENTATION.
* =========================================================================== */
/**
* @file
*
* @author ( MM Solutions AD )
*
*/
/* -----------------------------------------------------------------------------
*!
*! Revision History
*! ===================================
*! 05-Nov-2013 : Author ( MM Solutions AD )
*! Created
* =========================================================================== */
#include <stdint.h>
#include <stdlib.h>
#include <VcsHooksApi.h>
#include <DrvCmxDma.h>
#include <OsDrvCmxDma.h>
#include <OsDrvCpr.h>
#include <DrvDdr.h>

#include "initSystem.h"
//#include "sysconfig.h"
//include "hostinterface.h"
//include "comm.h"
//include "imgRecorder.h"
#include "debug.h"
//include "sysconfig.h"
#include "emmc.h"
//include "vi/vi_interface.h"
//#include "wdtAndFatalExtension.h"
#include "boardInit.h"
#include "rsTypes.h"

#ifdef ENABLE_JPEG
#include "leon/JpegEncoderApi.h"
#include "FrameTypes.h"
#endif

#ifdef ENABLE_JPEG
// JPEG encoding
// jpg buffer should max out at 8.25bits * width * height
#define YUVDATA_SIZE 1920*1080*3/2
uint8_t __attribute__((section(".ddr.bss"))) yuvdata[YUVDATA_SIZE];//jpgbuff[4208 * 3120 * 2];
uint8_t __attribute__((section(".ddr.bss"))) jpg_buff[1920 * 1080 * 2];//jpgbuff[4208 * 3120 * 2];
#define FRAME_BPP 1
// Define the remainng number of shaves to be used for jpeg encoding
#define NB_SHAVES_FOR_JPEGENC    JPEG_USED_SHV
#define BUFF_SIZE_SHAVE 100*1024//32*1024

static int jpeg_encode_cnt = 0;
#endif
/*
 * ****************************************************************************
 * ** Main ********************************************************************
 * ****************************************************************************
 */

#ifdef ENABLE_JPEG
int start_JpegCode()
{
	RS_IMG img;
    memset(&img,0,sizeof(RS_IMG));
#ifdef ENABLE_JPEG
    frameBuffer  frm_buffer;
    int t1, t2;
    char file_name[30] = {0};
#endif

    img.width = 1920;
    img.height = 1080;
    img.stride = 1920;
    img.imgSize = YUVDATA_SIZE;
    img.fmt = YUV420p;
    img.img = yuvdata;
    getFileFromEMMC(yuvdata,YUVDATA_SIZE,"yuv_in.data");

#ifdef ENABLE_JPEG
    frm_buffer.p1=img.img;
    frm_buffer.p2=img.img + (img.width * img.height);
    frm_buffer.p3=img.img + (img.width * img.height) + (img.width * img.height / 4);
    frm_buffer.spec.width=img.width;
    frm_buffer.spec.height=img.height;
    frm_buffer.spec.stride	= img.stride;
    frm_buffer.spec.bytesPP = FRAME_BPP;
    frm_buffer.spec.type	= YUV420p;
#endif
#ifdef ENABLE_JPEG
    printf("Start encoding JPEG\n");
    int jpgsize;
    printf("frame width %d,height %d.\n",frm_buffer.spec.width,frm_buffer.spec.height);
    t1 = GET_REG_WORD_VAL(TIM0_FREE_LOWER_RAW_ADR);
    jpgsize = JPEG_encode(frm_buffer, jpg_buff, NB_SHAVES_FOR_JPEGENC, BUFF_SIZE_SHAVE, JPEG_420_PLANAR);
    t2 = GET_REG_WORD_VAL(TIM0_FREE_LOWER_RAW_ADR);
    printf("Ended encoding JPEG, jpgsize=%d.\n", jpgsize);

    jpeg_encode_cnt++;
#if 1//debug
    if (jpeg_encode_cnt % 5 == 0)
    {
        printf("hehe.\n");//("[statistics]: jpeg encode frame %d, interval %5.2f ms\n", jpeg_encode_cnt, DrvTimerTicksToMs(t2 - t1));
    }
#endif
    sprintf(file_name,"capture_img%d.jpg",jpeg_encode_cnt);
    printf("save jpeg file name :%s\n",file_name);
    saveFileToEMMC((char *)(jpg_buff), jpgsize, file_name);
#endif


	return 0;
}
#endif
int main(int argc, char **argv)
{
    UNUSED(argc);
    UNUSED(argv);
    rtems_object_set_name(RTEMS_SELF, "rs_main");
    
    initSystem();
	//refreshWdt();
    #ifdef ENABLE_JPEG
    printf("here is start.\n");
    start_JpegCode();
    #endif	
    return 0;
}
