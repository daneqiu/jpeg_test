/******************************************************************************

 @File         : initSystem.h
 @Author       : MT
 @Brief        : IPIPE preprocessor Hw configuration  API
 Date          : 02 - Sep - 2014
 E-mail        : xxx.xx@movidius.com
 Copyright     : � Movidius Srl 2013, � Movidius Ltd 2014

 Description :


******************************************************************************/

#ifndef INITSYSTEM_LOS_H
#define INITSYSTEM_LOS_H

/**************************************************************************************************
 ~~~ Includes
 **************************************************************************************************/
#include <DrvCpr.h>

/**************************************************************************************************
 ~~~  Specific #defines and types (typedef,enum,struct)
 **************************************************************************************************/

#define BIGENDIANMODE               (0x01000786)

/**************************************************************************************************
 ~~~  Exported Functions
 **************************************************************************************************/

// ----------------------------------------------------------------------------
/// Setup all the clock configurations needed by this application and also the ddr
///
/// @return    0 on success, non-zero otherwise
int initClocksAndMemory(void);
void leonFlushShaveCache(void);
void initSystem(void);

#endif //APPHWCONFIG_LOS_H
