
#ifndef RS_TYPES_H__
#define RS_TYPES_H__

#include <stdint.h>

typedef enum rs_pixel_fmt{
	PIX_FORMAT_GRAY,
	PIX_FORMAT_BGR888,
	PIX_FORMAT_NV21,
	PIX_FORMAT_BGRA8888,
    PIX_FORMAT_I422,
    PIX_FORMAT_I420,
    PIX_FORMAT_YUV444,
    PIX_FORMAT_H264
}RS_PIXEL_FMT;

typedef struct rs_img 
{
	int width;				 
	int height;				
	int stride;				 
	int imgSize;			// 16 bytes. aligned
    union {                 // 32 bytes. aligned
        char capture_at[32];	
        struct 
        {
            int frameId;
            int hasFrameId;
            int reserved[6];
        } frameId;
    } timeUsage;
    RS_PIXEL_FMT fmt;		// 4 bytes
	int placeholder;	    // 4 bytes
	unsigned char* img;		// 4 bytes or 8 bytes depends on platform
							// total 64 bytes				
}__attribute__((aligned(16))) RS_IMG;

typedef struct FX_VI_CONFIG
{
    int width;
    int height;
    int fps;
    int idx;
}__attribute__((aligned(4))) FX_VI_CONFIG;


typedef enum FX_RUNMODE
{
    FX_RUNMODE_NONE        = 0,     /* none */
	FX_RUNMODE_LIVEVID     = 1,		/* live video mode*/
    FX_RUNMODE_FACECAP     = 2,   
	FX_RUNMODE_ENROLL      = 3,		/* face album management*/
	FX_RUNMODE_TRACK       = 4,		/* face track */
    FX_RUNMODE_FLOWSTA     = 5,
    FX_RUNMODE_INOUT       = 6,		/* head inout statistic */
	FX_RUNMODE_HEADTRACK   = 7,		/* head track */
    FX_RUNMODE_BODYTRACK   = 8,		/* body track */
	FX_RUNMODE_UNKNOWN     = 0x7F
}FX_RUNMODE;

typedef enum FX_TRACKMODE
{
	FX_RECOGNITION_HAS = 0,
	FX_RECOGNITION_NO
}FX_TRACKMODE;


typedef struct rs_point 
{
	float x;
	float y;
}__attribute__((aligned(4))) RS_POINT;


typedef struct rs_rect 
{
	int left;
	int top;
	int width;
	int height;
}__attribute__((aligned(4))) RS_RECT;

typedef struct fx_roi
{
    RS_RECT rect;
    int changed;
}__attribute__((aligned(4))) FX_ROI;

typedef struct SYS_VER
{
    int major;
    int minor;
    int revision;
}__attribute__((aligned(4))) SYS_VER;


#define FX_TRACKMODE_FUNC_FEATURE		(1<<0)
#define FX_TRACKMODE_FUNC_PID   		(1<<1)
#define FX_TRACKMODE_FUNC_CROP			(1<<2)
#define FX_TRACKMODE_FUNC_AGE_GENDER	(1<<3)
#define FX_TRACKMODE_FUNC_PID_ENROLLED	(1<<4)
#define FX_TRACKMODE_FUNC_LIVINGBODY    (1<<5)
#define FX_TRACKMODE_FUNC_BEAUTY        (1<<6)
#define FX_TRACKMODE_FUNC_PREVIEW   	(1<<31)

#endif