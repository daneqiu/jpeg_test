
#include "initConfig.h"

#define DESIRED_USB_FREQ_KHZ        (20000)
#if (DEFAULT_APP_CLOCK_KHZ%DESIRED_USB_FREQ_KHZ)
#error "Can not achieve USB Reference frequency. Aborting."
#endif

tyAuxClkDividerCfg auxClkAllOn[] =
{
    {AUX_CLK_MASK_UART, CLK_SRC_REFCLK0, 96, 625},   // Give the UART an SCLK that allows it to generate an output baud rate of of 115200 Hz (the uart divides by 16)
    {
        .auxClockEnableMask     = ( AUX_CLK_MASK_I2S0  |     \
                                AUX_CLK_MASK_I2S1  |     \
                                AUX_CLK_MASK_I2S2  ),                // all rest of the necessary clocks
        .auxClockSource         = CLK_SRC_SYS_CLK,            //
        .auxClockDivNumerator   = 1,                          //
        .auxClockDivDenominator = 1,                          //
    },
    // {
    //     .auxClockEnableMask     = AUX_CLK_MASK_LCD ,          // LCD clock
    //     .auxClockSource         = CLK_SRC_SYS_CLK_DIV2,
    //     .auxClockDivNumerator   = 1,
    //     .auxClockDivDenominator = 1
    // },
    // {
    //     .auxClockEnableMask     = AUX_CLK_MASK_MEDIA,         // SIPP Clock
    //     .auxClockSource         = CLK_SRC_SYS_CLK ,      //
    //     .auxClockDivNumerator   = 1,                          //
    //     .auxClockDivDenominator = 1,                          //
    // },
    //TODO: Disable CIF for release application?
#if VI_xc7022_lrt
    // haan update for xc7022 mipi input, TODO: need merge the config with bt1120
    {
        .auxClockEnableMask     = (AUX_CLK_MASK_CIF0 | AUX_CLK_MASK_CIF1),  // CIFs Clock
        .auxClockSource         = CLK_SRC_SYS_CLK, //CLK_SRC_SYS_CLK_DIV2,//CLK_SRC_SYS_CLK,//modify by lhf 171108
        .auxClockDivNumerator   = 1,                          //
        .auxClockDivDenominator = 1, //20,//1,                          //modify by lhf 171108
    },
#else
    // lfh update for bt1120 input
    {
        .auxClockEnableMask     = (AUX_CLK_MASK_CIF1),  // CIFs Clock
        .auxClockSource         = CLK_SRC_SYS_CLK_DIV2,//CLK_SRC_SYS_CLK,//modify by lhf 171108
        .auxClockDivNumerator   = 1,                          //
        .auxClockDivDenominator = 20,//1,                          //modify by lhf 171108
    },
#endif
    {
        .auxClockEnableMask     = (AUX_CLK_MASK_MIPI_ECFG | AUX_CLK_MASK_MIPI_CFG),             // MIPI CFG + ECFG Clock
        .auxClockSource         = CLK_SRC_SYS_CLK     ,       //
        .auxClockDivNumerator   = 1,                          //
        .auxClockDivDenominator = 30, //(uint32_t)(DEFAULT_APP_CLOCK_KHZ/24000),                         //
    },
    // {
    //     .auxClockEnableMask = AUX_CLK_MASK_MIPI_TX0 | AUX_CLK_MASK_MIPI_TX1,
    //     .auxClockSource = CLK_SRC_REFCLK0,
    //     .auxClockDivNumerator = 1,
    //     .auxClockDivDenominator = 1
    // }, // ref clocks for MIPI PLL,
    {
        .auxClockEnableMask = (u32)(1 << CSS_AUX_TSENS),
        .auxClockSource = CLK_SRC_REFCLK0,
        .auxClockDivNumerator = 1,
        .auxClockDivDenominator = 10,
    },
    // {
    //   .auxClockEnableMask     = AUX_CLK_MASK_USB_PHY_EXTREFCLK,
    //   .auxClockSource         = CLK_SRC_PLL0,
    //   .auxClockDivNumerator   = 1,
    //   .auxClockDivDenominator = (uint32_t)(DEFAULT_APP_CLOCK_KHZ/DESIRED_USB_FREQ_KHZ)
    // },
    /*{
      .auxClockEnableMask     = AUX_CLK_MASK_USB_PHY_REF_ALT_CLK,
      .auxClockSource         = CLK_SRC_PLL0,
      .auxClockDivNumerator   = 1,
      .auxClockDivDenominator = (uint32_t)(DEFAULT_APP_CLOCK_KHZ/DESIRED_USB_FREQ_KHZ)
    },*/
    // {
    //   .auxClockEnableMask     = AUX_CLK_MASK_USB_CTRL_REF_CLK,
    //   .auxClockSource         = CLK_SRC_PLL0,
    //   .auxClockDivNumerator   = 1,
    //   .auxClockDivDenominator = (uint32_t)(DEFAULT_APP_CLOCK_KHZ/DESIRED_USB_FREQ_KHZ)
    // },
    {
      .auxClockEnableMask     = AUX_CLK_MASK_USB_CTRL_SUSPEND_CLK,
      .auxClockSource         = CLK_SRC_PLL0,
      .auxClockDivNumerator   = 1,
      .auxClockDivDenominator = (uint32_t)(DEFAULT_APP_CLOCK_KHZ/DESIRED_USB_FREQ_KHZ)
    },
	//{AUX_CLK_MASK_SDIO, CLK_SRC_PLL0, 1, 12/*6*/},   		// SDIO
	{AUX_CLK_MASK_SDIO, CLK_SRC_PLL0, 1, 6},   		// SDIO
    {0,0,0,0}, // Null Terminated List
};
