#ifndef _INIT_CONFIG_HH
#define _INIT_CONFIG_HH

#include <OsDrvCpr.h>
#include <DrvGpio.h>
#include "DrvCpr.h"
#include <DrvDdr.h>
#include <OsDrvTimer.h>
#include "DrvTimer.h"
#include "swcLeonUtils.h"
#include <OsDrvSvu.h>
#include <OsDrvCmxDma.h>
#include <OsDrvShaveL2Cache.h>
#include <DrvShaveL2Cache.h>
#include "initSystem.h"
#include "brdGpioCfgs/brdMv0182GpioDefaults.h"
#include "boardInit.h"
#include <myriad2_version.h>

extern tyAuxClkDividerCfg auxClkAllOn[];

#define SYS_CLK_KHZ 12000 // 12MHz

#define DEFAULT_APP_CLOCK_KHZ   480000

#define CSS_CLOCKS          ( DEFAULT_CORE_CSS_DSS_CLOCKS |  \
                                             DEV_CSS_USB |  \
                                             DEV_CSS_USB_APBSLV | \
                                             DEV_CSS_SDIO | \
                                             DEV_CSS_I2C0 | \
                                             DEV_CSS_I2C1 | \
                                             DEV_CSS_I2C2 | \
                                             DEV_CSS_SPI0 | \
                                             DEV_CSS_SPI1 | \
                                             DEV_CSS_SPI2 \
                                              )
/** in common/drivers/myriad2/socDrivers/leon/hgl/arch/ma2x5x/include/HglCprDefinesMa2x5x.h
  #define DEFAULT_CORE_CSS_DSS_CLOCKS    ( DEV_CSS_LOS             |    \
                                         DEV_CSS_LAHB_CTRL       |    \
                                         DEV_CSS_APB4_CTRL       |    \
                                         DEV_CSS_CPR             |    \
                                         DEV_CSS_ROM             |    \
                                         DEV_CSS_LOS_L2C         |    \
                                         DEV_CSS_MAHB_CTRL       |    \
                                         DEV_CSS_LOS_ICB         |    \
                                         DEV_CSS_LOS_DSU         |    \
                                         DEV_CSS_LOS_TIM         |    \
                                         DEV_CSS_GPIO            |    \
                                         DEV_CSS_JTAG            |    \
                                         DEV_CSS_APB1_CTRL       |    \
                                         DEV_CSS_AHB_DMA         |    \
                                         DEV_CSS_APB3_CTRL       |    \
                                         DEV_CSS_UART            |    \
                                         DEV_CSS_SAHB_CTRL       |    \
                                         DEV_CSS_MSS_MAS         |    \
                                         DEV_CSS_UPA_MAS         |    \
                                         DEV_CSS_DSS_APB         |    \
                                         DEV_CSS_DSS_APB_RST_PHY |    \
                                         DEV_CSS_DSS_APB_RST_CTRL|    \
                                         DEV_CSS_DSS_BUS         |    \
                                         DEV_CSS_DSS_BUS_MAHB    |    \
                                         DEV_CSS_DSS_BUS_DXI     |    \
                                         DEV_CSS_DSS_BUS_AAXI    |    \
                                         DEV_CSS_DSS_BUS_MXI     |    \
                                         DEV_CSS_LAHB2SHB        |    \
                                         DEV_CSS_SAHB2MAHB       |    \
                                         DEV_CSS_AON                      )
 */
/* CSS_CLOCKS disabled:
DEV_CSS_BIST
DEV_CSS_GETH
DEV_CSS_I2S0
DEV_CSS_I2S1
DEV_CSS_I2S2
*/

#define MSS_CLOCKS         (  DEV_MSS_APB_SLV     |     \
                              DEV_MSS_APB2_CTRL   |     \
                              DEV_MSS_RTBRIDGE    |     \
                              DEV_MSS_RTAHB_CTRL  |     \
                              DEV_MSS_AXI_BRIDGE  |     \
                              DEV_MSS_MXI_CTRL    |     \
                              DEV_MSS_MXI_DEFSLV  |     \
                              DEV_MSS_AXI_MON     |     \
                              DEV_MSS_AMC         |     \
                              DEV_MSS_TIM         |     \
                              DEV_MSS_CIF1        |     \
                              DEV_MSS_LRT_ICB     | \
                              DEV_MSS_LRT         |\
                              DEV_MSS_LRT_DSU     |  \
                              DEV_MSS_LRT_L2C     \
                                 )
/*  MSS_CLOCKS disabled:
DEV_MSS_LCD 
DEV_MSS_NAL  
DEV_MSS_CIF0 
DEV_MSS_SIPP 
DEV_MSS_LRT 
DEV_MSS_LRT_DSU 
DEV_MSS_LRT_L2C 
DEV_MSS_LRT_ICB*/

#define SIPP_CLOCKS         (DEV_SIPP_MIPI               | \
DEV_SIPP_MIPI_RX0           | \
DEV_SIPP_MIPI_RX1           | \
DEV_SIPP_MIPI_RX2           | \
DEV_SIPP_MIPI_RX3            \
 )
 /* SIPP_CLOCKS: disabled
DEV_SIPP_LSC                | \
DEV_SIPP_MIPI               | \
DEV_SIPP_MIPI_TX0           | \
DEV_SIPP_MIPI_TX1           | \
DEV_SIPP_MIPI_RX0           | \
DEV_SIPP_MIPI_RX1           | \
DEV_SIPP_MIPI_RX2           | \
DEV_SIPP_MIPI_RX3           | \
DEV_SIPP_SIPP_ABPSLV        | \
DEV_SIPP_SIGMA              | \
DEV_SIPP_RAW                | \
DEV_SIPP_DBYR               | \
DEV_SIPP_DOGL               | \
DEV_SIPP_LUMA               | \
DEV_SIPP_SHARPEN            | \
DEV_SIPP_CGEN               | \
DEV_SIPP_MED                | \
DEV_SIPP_CHROMA             | \
DEV_SIPP_CC                 | \
DEV_SIPP_LUT                | \
DEV_SIPP_EDGE_OP            | \
DEV_SIPP_CONV               | \
DEV_SIPP_HARRIS             | \
(1<<MSS_SP_SIPP)            | \
DEV_SIPP_UPFIRDN0           | \
DEV_SIPP_UPFIRDN1           | \
DEV_SIPP_UPFIRDN2           | \
DEV_SIPP_SIPP_ABPSLV        | \
DEV_SIPP_APB_SLV             \    
*/

#define UPA_CLOCKS             (0) 

/*
DEV_UPA_SHAVE_L2
DEV_UPA_CDMA
DEV_UPA_CTRL
*/

#endif