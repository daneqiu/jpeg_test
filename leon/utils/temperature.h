
#ifndef RS_TEMERATURE_H__
#define RS_TEMERATURE_H__

int initTemperature();
int readTemperature(float *css, float *mss, float *upa1, float *upa2);

#endif