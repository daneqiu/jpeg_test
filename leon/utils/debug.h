#ifndef YK_DEBUG_H
#define YK_DEBUG_H

#include <stdio.h>

#ifndef DEBUG_VERBOSE
#define DEBUG_VERBOSE 0
#endif

#if DEBUG_VERBOSE == 1
#warning "Debug print is opened"
#endif

#if DEBUG_VERBOSE
#define MY_PRINT(s, ...)	printf("%s:%s:%d: " s, __FILE__, __func__, __LINE__, ##__VA_ARGS__)
#else
#define MY_PRINT(s, ...)
#endif


void print_heap_info();

#endif