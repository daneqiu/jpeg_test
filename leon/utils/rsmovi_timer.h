#ifndef TIME_MEASURE_H__
#define TIME_MEASURE_H__

#include <OsDrvTimer.h>
#include <OsDrvCpr.h>

typedef struct RSMoviTimer
{
	tyTimeStamp timer_data_local;
}RSMoviTimer;

int rsmoviTimer_refresh(RSMoviTimer * timer);
double rsmoviTimer_elapsed(RSMoviTimer * timer);

#endif