/**
 * @file
 *
 * @brief CPU Usage Report
 * @ingroup libmisc_cpuuse CPU Usage
 */

/*
 *  COPYRIGHT (c) 1989-2010.
 *  On-Line Applications Research Corporation (OAR).
 *
 *  Modified to separate data gathering from printing.
 *  COPYRIGHT (c) 2016.
 *  Movidius.
 *
 *  The license and distribution terms for this file may be
 *  found in the file LICENSE in this distribution or at
 *  http://www.rtems.org/license/LICENSE.
 */

#ifdef HAVE_CONFIG_H
#include "config.h"
#endif

#include <string.h>
#include <stdlib.h>
#include <stdio.h>
#include <ctype.h>
#include <inttypes.h>

#include <rtems/cpuuse.h>
#include <rtems/printer.h>
#include <rtems/score/threadimpl.h>
#include <rtems/score/todimpl.h>

#include <cpuuse2.h>

// Only for debug: set to 1 to enable original print code
#define PRINT_CPU_USE 0

typedef struct {
  const rtems_printer *printer;
  struct cpu_usage    *usage;
  Timestamp_Control    total;
  Timestamp_Control    uptime_at_last_reset;
} cpu_usage_context;

static bool cpu_usage_visitor2( Thread_Control *the_thread, void *arg )
{
  cpu_usage_context *ctx;
  char               name[ 38 ];
  uint32_t           ival;
  uint32_t           fval;
  Timestamp_Control  uptime;
  Timestamp_Control  used;
  uint32_t           seconds;
  uint32_t           nanoseconds;

  ctx = arg;
#if 0 // Original RTEMS implementation
      // BROKEN when paired with rtems_object_set_name
  _Thread_Get_name( the_thread, name, sizeof( name ) );
#else
  rtems_object_get_name( the_thread->Object.id, sizeof( name ), name );
#endif

  _Thread_Get_CPU_time_used( the_thread, &used );
  _TOD_Get_uptime( &uptime );
  _Timestamp_Subtract( &ctx->uptime_at_last_reset, &uptime, &ctx->total );
  _Timestamp_Divide( &used, &ctx->total, &ival, &fval );
  seconds = _Timestamp_Get_seconds( &used );
  nanoseconds = _Timestamp_Get_nanoseconds( &used ) /
    TOD_NANOSECONDS_PER_MICROSECOND;

  struct cpu_usage *usage = ctx->usage;
  struct thread_cpu_usage *current;
  usage->threads = realloc(usage->threads, (usage->num_threads + 1) * sizeof (struct thread_cpu_usage));
  current = &usage->threads[usage->num_threads];
  current->id = the_thread->Object.id;
  strcpy(current->name, name);
  current->ran_seconds = seconds;
  current->ran_microsec = nanoseconds;
  current->percent_int = ival;
  current->percent_float = fval;
  current->priority = the_thread->Real_priority.priority;
  current->state = the_thread->current_state;
  (usage->num_threads)++;

  if (PRINT_CPU_USE)
  rtems_printf(
    ctx->printer,
    " 0x%08" PRIx32 " | %-38s |"
      "%7" PRIu32 ".%06" PRIu32 " |%4" PRIu32 ".%03" PRIu32 "\n",
    the_thread->Object.id,
    name,
    seconds, nanoseconds,
    ival, fval
  );

  return false;
}

/*
 *  rtems_cpu_usage_report
 */
void cpu_usage_get_data(
  struct cpu_usage *usage
)
{
  cpu_usage_context  ctx;
  uint32_t           seconds;
  uint32_t           nanoseconds;

  rtems_printer printer_alloc;
  rtems_printer *printer = &printer_alloc;
  if (PRINT_CPU_USE) {
    rtems_print_printer_printk( printer );
    ctx.printer = printer;
  }

  usage->num_threads = 0;
  usage->threads = NULL;

  ctx.usage = usage;

  /*
   *  When not using nanosecond CPU usage resolution, we have to count
   *  the number of "ticks" we gave credit for to give the user a rough
   *  guideline as to what each number means proportionally.
   */
  _Timestamp_Set_to_zero( &ctx.total );
  ctx.uptime_at_last_reset = CPU_usage_Uptime_at_last_reset;

  if (PRINT_CPU_USE)
  rtems_printf(
     printer,
     "-------------------------------------------------------------------------------\n"
     "                              CPU USAGE BY THREAD\n"
     "------------+----------------------------------------+---------------+---------\n"
     " ID         | NAME                                   | SECONDS       | PERCENT\n"
     "------------+----------------------------------------+---------------+---------\n"
  );

  rtems_task_iterate( cpu_usage_visitor2, &ctx );

  seconds = _Timestamp_Get_seconds( &ctx.total );
  nanoseconds = _Timestamp_Get_nanoseconds( &ctx.total ) /
    TOD_NANOSECONDS_PER_MICROSECOND;

  usage->total_seconds = seconds;
  usage->total_microsec = nanoseconds;

  if (PRINT_CPU_USE)
  rtems_printf(
     printer,
     "------------+----------------------------------------+---------------+---------\n"
     " TIME SINCE LAST CPU USAGE RESET IN SECONDS:                    %7" PRIu32 ".%06" PRIu32 "\n"
     "-------------------------------------------------------------------------------\n",
     seconds, nanoseconds
  );
}

static char* task_state_str(uint32_t state)
{
    if (state == 0)
    {
        return "READY";
    }
    else
    {
        if      (state & STATES_WAITING_FOR_MUTEX             ) return "waitMutex";
        else if (state & STATES_WAITING_FOR_SEMAPHORE         ) return "waitSemaphore";
        else if (state & STATES_WAITING_FOR_EVENT             ) return "waitEvent";
        else if (state & STATES_WAITING_FOR_SYSTEM_EVENT      ) return "waitSystemEvent";
        else if (state & STATES_WAITING_FOR_MESSAGE           ) return "waitMessage";
        else if (state & STATES_WAITING_FOR_CONDITION_VARIABLE) return "waitConditionVar";
        else if (state & STATES_WAITING_FOR_FUTEX             ) return "waitFutex";
        else if (state & STATES_WAITING_FOR_BSD_WAKEUP        ) return "waitBsdWakeup";
        else if (state & STATES_WAITING_FOR_TIME              ) return "waitTime";
        else if (state & STATES_WAITING_FOR_PERIOD            ) return "waitPeriod";
        else if (state & STATES_WAITING_FOR_SIGNAL            ) return "waitSignal";
        else if (state & STATES_WAITING_FOR_BARRIER           ) return "waitBarrier";
        else if (state & STATES_WAITING_FOR_RWLOCK            ) return "waitRwLock";
        else if (state & STATES_WAITING_FOR_JOIN_AT_EXIT      ) return "waitJoinAtExit";
        else if (state & STATES_WAITING_FOR_JOIN              ) return "waitJoin";
        else if (state & STATES_SUSPENDED                     ) return "suspended";
        else if (state & STATES_WAITING_FOR_SEGMENT           ) return "waitSegment";
        else if (state & STATES_LIFE_IS_CHANGING              ) return "lifeIsChanging";
        else if (state & STATES_DEBUGGER                      ) return "debugger";
        else if (state & STATES_INTERRUPTIBLE_BY_SIGNAL       ) return "interrBySignal";
        else if (state & STATES_WAITING_FOR_RPC_REPLY         ) return "waitRpcReply";
        else if (state & STATES_ZOMBIE                        ) return "ZOMBIE";
        else                                                    return "UNKNOWN";
    }
    return "!never!";
}

void cpu_usage_print_report( struct cpu_usage *usage, bool detailed )
{
    uint32_t i;
    struct cpu_usage usg;

    if (usage == NULL)
    {
        usage = &usg;
        cpu_usage_get_data(usage);
    }

    if (detailed)
    {
        printf("\nCPU usage by thread:\n");
        printf("        ID Name                Seconds       %% Prio State\n");
    }
    for(i = 0; i < usage->num_threads; i++)
    {
        if (detailed)
        {
            printf("0x%08"PRIx32" %-16s %3"PRIu32".%06"PRIu32" %3"PRIu32".%03"PRIu32"  %3"PRIu32" %s\n",
                    usage->threads[i].id,
                    usage->threads[i].name,
                    usage->threads[i].ran_seconds, usage->threads[i].ran_microsec,
                    usage->threads[i].percent_int, usage->threads[i].percent_float,
                    usage->threads[i].priority,
                    task_state_str(usage->threads[i].state)
                    );
        }
        else
        {
            printf("%3"PRIu32".%03"PRIu32",",
                    usage->threads[i].percent_int, usage->threads[i].percent_float
                    );
        }
    }
    if (detailed)
        printf("Time since last usage reset %3"PRIu32".%06"PRIu32" seconds\n\n", usage->total_seconds, usage->total_microsec);
    else
        printf("%3"PRIu32".%06"PRIu32"\n", usage->total_seconds, usage->total_microsec);

    free(usage->threads);
}
