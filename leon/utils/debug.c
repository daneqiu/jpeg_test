
#include <stdlib.h>
#include <stdio.h>
#include <rtems.h>
#include <rtems/malloc.h>

extern void rtems_shell_print_unified_work_area_message(void);
extern void rtems_shell_print_heap_info(
  const char             *c,
  const Heap_Information *h
);

extern void rtems_shell_print_heap_stats(
  const Heap_Statistics *s
);

void print_heap_info()
{
    region_information_block info;
    malloc_info( &info );
    rtems_shell_print_heap_info( "free", &info.Free );
    rtems_shell_print_heap_info( "used", &info.Used );
    rtems_shell_print_heap_stats( &info.Stats );
}