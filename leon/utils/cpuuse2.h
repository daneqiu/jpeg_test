/**
 * @file rtems/cpuuse.h
 * 
 * @defgroup libmisc_cpuuse CPU Usage
 *
 * @ingroup libmisc
 * @brief CPU Usage Report
 *
 * This include file contains information necessary to utilize
 * and install the cpu usage reporting mechanism.
 */

/*
 * COPYRIGHT (c) 1989-2011.
 * On-Line Applications Research Corporation (OAR).
 *
 * Modified to separate data gathering from printing.
 * COPYRIGHT (c) 2016.
 * Movidius.
 *
 * The license and distribution terms for this file may be
 * found in the file LICENSE in this distribution or at
 * http://www.rtems.org/license/LICENSE.
 */

#ifndef __RTEMS_CPUUSE2_h
#define __RTEMS_CPUUSE2_h

#include <rtems.h>
#include <rtems/bspIo.h>

#include <rtems/score/timestamp.h>

/**
 *  @defgroup libmisc_cpuuse CPU Usage
 *
 *  @ingroup libmisc
 */
/**@{*/
#ifdef __cplusplus
extern "C" {
#endif

extern Timestamp_Control CPU_usage_Uptime_at_last_reset;

struct thread_cpu_usage
{
    Objects_Id id;
    char name[32];
    uint32_t ran_seconds;
    uint32_t ran_microsec;
    uint32_t percent_int;
    uint32_t percent_float;
    uint32_t priority;
    uint32_t state;
};

struct cpu_usage
{
    uint32_t num_threads;
    struct thread_cpu_usage *threads;
    // Total since last reset
    uint32_t total_seconds;
    uint32_t total_microsec;
};

/*
 * cpu_usage_get_data
 */

void cpu_usage_get_data(
  struct cpu_usage *usage
);

/**
 *  @brief Report CPU usage.
 *
 *  CPU Usage Reporter
 *
 *  usage:   previously populated by cpu_usage_get_data
 *           or NULL, to get the data before reporting
 *  details: true  - formatted print
 *           false - usage percents for threads, followed by total CPU time
 */

void cpu_usage_print_report(
  struct cpu_usage *usage,
  bool details
);

#ifdef __cplusplus
}
#endif
/**@}*/
#endif
/* end of include file */
