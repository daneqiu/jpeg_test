#ifdef __RTEMS__

#include <rtems.h>
#include <rtems/bspIo.h>
#include "printToBuf.h"

#if REDIRECT_PRINTF_TO_RAM
#define PRINT_LOG_BUF_SIZE  1024*1024*2
static char printlog_buf[PRINT_LOG_BUF_SIZE];
static int printlog_buf_ptr_wr = 0;
static int printlog_buf_ptr_rd = 0;

static void buff_out_char_to_mem(char chr)
{
    printlog_buf[printlog_buf_ptr_wr] = chr;
    printlog_buf_ptr_wr++;
    if (printlog_buf_ptr_wr >= PRINT_LOG_BUF_SIZE)
    {
        printlog_buf_ptr_wr = 0;
    }
}

BSP_output_char_function_type BSP_output_char         = buff_out_char_to_mem;


void printToBuf_fetchLog(char ** log, int * size)
{
    *log = &printlog_buf[printlog_buf_ptr_rd];
    int wrPtr = printlog_buf_ptr_wr;
    if (wrPtr < printlog_buf_ptr_rd)
    {
        wrPtr = PRINT_LOG_BUF_SIZE;
    }
    *size = wrPtr - printlog_buf_ptr_rd;
    printlog_buf_ptr_rd = wrPtr;
    if (printlog_buf_ptr_rd >= PRINT_LOG_BUF_SIZE)
    {
        printlog_buf_ptr_rd = 0;
    }
}

void printToBuf_clear()
{
    printlog_buf_ptr_rd = printlog_buf_ptr_wr;
}
#else

void printToBuf_fetchLog(char ** log, int * size)
{
    *size = 0;   
}
void printToBuf_clear()
{

}

#endif

#endif

