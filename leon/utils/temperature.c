
#include <OsDrvCpr.h>

int initTemperature()
{
    SET_REG_WORD(AON_TSENS_TRIM_CFG_ADR,0x0d0d0d0d);
    SET_REG_WORD(AON_TSENS_CFG_ADR,0x80F0);
    SET_REG_WORD(AON_TSENS_CFG_ADR,0x80FF);
    return 0;
}


int readTemperature(float *css, float *mss, float *upa1, float *upa2)
{
    // Start reading DATA0, CSS & MSS
    u32 tempdata=0;
    tempdata = GET_REG_WORD_VAL(AON_TSENS_DATA0);
    // printf("MSS & CSS temp is 0x%X \n", tempdata);
    u32 tempdata_MSB=0;
    u32 tempdata_LSB=0;
    tempdata_MSB= (tempdata>>16) & 0x3ff;
    tempdata_LSB= tempdata & 0x3ff;
    float NR=0.0f;
    NR= (float)tempdata_MSB;
    NR= (-1.6743E-11)*NR*NR*NR*NR +
        (+8.1542E-08)*NR*NR*NR +
        (-1.8201E-04)*NR*NR +
        (+3.1020E-01)*NR +
        (-4.8380E+01);
    printf("------Start Reading Temp Sensor ----\n");
    printf("CSS is %2.1f\n", NR);
	*css = NR;
		
    NR= (float)tempdata_LSB;
    NR= (-1.6743E-11)*NR*NR*NR*NR +
        (+8.1542E-08)*NR*NR*NR +
        (-1.8201E-04)*NR*NR +
        (+3.1020E-01)*NR +
        (-4.8380E+01);
    printf("MSS is %2.1f\n", NR);
	*mss = NR;
		
    // Start reading DATA1, UPA1 & 0
    tempdata = GET_REG_WORD_VAL(AON_TSENS_DATA1);
    tempdata_MSB= (tempdata>>16) & 0x3ff;
    tempdata_LSB= tempdata & 0x3ff;
    NR= (float)tempdata_MSB;
    NR= (-1.6743E-11)*NR*NR*NR*NR +
        (+8.1542E-08)*NR*NR*NR +
        (-1.8201E-04)*NR*NR +
        (+3.1020E-01)*NR +
        (-4.8380E+01);
    printf("UPA1 is %2.1f\n", NR);
	*upa1 = NR;
		
    NR= (float)tempdata_LSB;
    NR= (-1.6743E-11)*NR*NR*NR*NR +
        (+8.1542E-08)*NR*NR*NR +
        (-1.8201E-04)*NR*NR +
        (+3.1020E-01)*NR +
        (-4.8380E+01);
    printf("UPA1 is %2.1f\n", NR);
	*upa2 = NR;
	
    printf("-----Finish Reading Temp Sensor ----\n");
    return 0;
}