#ifndef __PRINT_TO_BUF_HH__
#define __PRINT_TO_BUF_HH__

#ifndef REDIRECT_PRINTF_TO_RAM
#define REDIRECT_PRINTF_TO_RAM  0
#endif

void printToBuf_fetchLog(char ** log, int * size);
void printToBuf_clear();

#endif