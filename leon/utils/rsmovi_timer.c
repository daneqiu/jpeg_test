#include "rsmovi_timer.h"

int rsmoviTimer_refresh(RSMoviTimer * timer)
{
	if (timer == NULL)
	{
		return -1;
	}
	OsDrvTimerStartTicksCount(&(timer->timer_data_local));
	return 0;
}

double rsmoviTimer_elapsed(RSMoviTimer * timer)
{
	if (timer == NULL)
	{
		return 0.0;
	}
    u64 cyclesElapsed_local;
    double usedMs;
	u32 clocksPerUs;
	OsDrvTimerGetElapsedTicks(&(timer->timer_data_local), &cyclesElapsed_local);
	OsDrvCprGetSysClockPerUs(&clocksPerUs);
	usedMs = cyclesElapsed_local / 1000.0 / clocksPerUs;
    return usedMs;
}
