#include <rtems.h>
#include <fcntl.h>
#include <errno.h>
#include <DrvTimer.h>
#include <OsDrvTimer.h>
#include "OsDrvSpiBus.h"
#include "DrvGpio.h"
#include "OsDrvCpr.h"
#include <SpiFlashN25QDevice.h>
#include "boardInit.h"
#include <sys/ioctl.h>
#include <DrvUart.h>
#include <stdlib.h>
#include <swcCrc.h>
 
#if 1

/// check DECLARE_SPI_BUS description before calling
DECLARE_SPI_BUS_GPIO_SS(myr2_spi_0, 1, 0, 0, 1, 1, 10*1000*1000, 8);

void myr2_spi_test_pin_config(void)
{
    DrvGpioModeRange(74, 76, D_GPIO_MODE_0);
    DrvGpioModeRange(77, 77, D_GPIO_MODE_0 | D_GPIO_DIR_OUT);
    // DrvGpioModeRange(78, 79, 5);
}


int initUart(int baudrate)
{
    // Input clock selection
    tyClockType   inClkType;
    tyCprClockSrc inClkSrc;
    if (1)
    {
        inClkType = PLL0_OUT_CLK;
        inClkSrc  = CLK_SRC_PLL0;
    }
    else
    {
        inClkType = REFCLK0;
        inClkSrc  = CLK_SRC_REFCLK0;
    }

    int desiredClk = baudrate * 16;

    s32 inputClkKhz = -1;
    OsDrvCprGetClockFreqKhz(inClkType, NULL, &inputClkKhz);
    if (inputClkKhz == -1)
    {
        printf("Error getting input clock\n");
        return -1;
    }
    int64_t inputClk = inputClkKhz * 1000;

    unsigned smallestError = -1u, bestNum = 1, bestDen = 1;
    // num, den can be max 4095 (12 bits)
    // constraint: (num == den) or (2*num <= den)
    if (desiredClk != inputClk)
    {
        for (int num = 1; num < 2048; num++)
        {
            for (int den = 2 * num; den < 4096; den++)
            {
                unsigned error = abs(inputClk*num/den - desiredClk);
                if (error < smallestError)
                {
                    smallestError = error;
                    bestNum = num;
                    bestDen = den;
                }
                if (error == 0)
                    break; // Exact match
            }
        }
    }

    // Print debug info and wait for debugger to read it from UART RX
    // FIFO (loopback mode) before reinitializing UART
    if (0) // <--- Set to 1
    {
        printf("Desired %d, achieved %lld x %d/%d / 16 = %lld\n", baudrate,
            inputClk, bestNum, bestDen, inputClk*bestNum/bestDen/16);
        usleep(1500*1000); // 1.5s
    }

    OsDrvCprSysDeviceAction(CSS_DOMAIN, ENABLE_CLKS, DEV_CSS_UART);
    OsDrvCprAuxClockEnable(AUX_CLK_MASK_UART, inClkSrc, bestNum, bestDen);

    tDrvUartCfg uartCfg = {
            .DataLength = UartDataBits8,
            .StopBits   = UartStopBit1,
            .Parity     = UartParityNone,
            .BaudRate   = baudrate,
            .LoopBack   = No
    };

    // We pass desiredClk (not achievedClk) for internal checks to succeed
    int sc = DrvUartInit(&uartCfg, desiredClk);
    if (sc != D_UART_INIT_FUNC_STATUS_OK)
    {
        printf("UART init error\n");
        return -1;
    }

#if 0
    // mwd board uart config
    // Configure the used GPIOs depending on board design
    DrvGpioSetMode(15, D_GPIO_MODE_1); // UART TX
    // UART RX pin is not required (only needed if e.g. shell is used)
    DrvGpioSetMode(14, D_GPIO_MODE_4); // UART RX
#else
    // fx board uart config
    DrvGpioSetMode(2, D_GPIO_MODE_3); // UART TX
	// UART RX pin is not required (only needed if e.g. shell is used)
	DrvGpioSetMode(3, D_GPIO_MODE_3); // UART RX
#endif

    return 0;
}


/*=========================================================================*\
  | Function:                                                                 |
  \*-------------------------------------------------------------------------*/
int myr2_register_libi2c_spi_bus(void)

{
    int ret_code;
    int spi0_busno;

    /*
     * init I2C library (if not already done)
     */
    if((ret_code = rtems_libi2c_initialize()) != 0)
    {
        printf("rtems_libi2c_initialize FAILED %d \n", ret_code);
    }

    /*
     * register first I2C bus
     */

    ret_code = rtems_libi2c_register_bus(SPI_BUS_NAME,
                                         (rtems_libi2c_bus_t *)&myr2_spi_0);
    if (ret_code < 0)
    {
        printf("Could not register the bus\n");
        return ret_code;
    }
    spi0_busno = ret_code;

    // Returns minor
    ret_code = rtems_libi2c_register_drv(SPI_FLASH_NAME,
                                         spi_flash_N25Q_driver_descriptor,
                                         spi0_busno, 77);
    if (ret_code < 0)
    {
        printf("Could not register the spi device\n");
    }

    return 0;
}
#endif
