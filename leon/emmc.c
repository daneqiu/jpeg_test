///
/// @file
/// @copyright All code copyright Movidius Ltd 2012, all rights reserved.
///            For License Warranty see: common/license.txt
///
/// @brief     main leon file
///

// 1: Includes
// ----------------------------------------------------------------------------
#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include <rtems.h>
#include <fcntl.h>
#include <mv_types.h>
#include <rtems/fsmount.h>
#include <rtems/bdpart.h>
#include <rtems/dosfs.h>
#include <OsDrvCpr.h>
#include <OsDrvSdio.h>
#include <OsBrdMv0182.h>
#include <DrvGpio.h>
#include <assert.h>

#include <OsDrvTimer.h>

#include <registersMyriad.h>
#include <stdio.h>
#include <DrvGpio.h>
#include <DrvRegUtils.h>
#include <fcntl.h>
#include <dirent.h>

#include "emmc.h"

// Perform an MMC / SDcard format
#define FORMAT_CARD
#define MMC_BUILD

// 2:  Source Specific #defines and types  (typedef, enum, struct)
// ----------------------------------------------------------------------------
#define BUFFER_SIZE 					(1024*1024*5)
#define DEFAULT_SDIO_INT_PRIORITY		10
#define SDIO_SLOT_USED					0
#define SDIO_DEVNAME_USED				"/dev/sdc0"

// 3: Global Data (Only if absolutely necessary)
// ----------------------------------------------------------------------------

// 4: Static Local Data
// ----------------------------------------------------------------------------
/* Sections decoration is require here for downstream tools */
/* Mount Table */
static const rtems_fstab_entry fs_table [] = {
   {
     .source = "/dev/sdc0",
     .target = "/mnt/sdcard",
     .type = "dosfs",
     .options = RTEMS_FILESYSTEM_READ_WRITE,
     .report_reasons = RTEMS_FSTAB_NONE,
     .abort_reasons = RTEMS_FSTAB_OK
    }, 
	{
     .source = "/dev/sdc01",
     .target = "/mnt/sdcard",
     .type = "dosfs",
     .options = RTEMS_FILESYSTEM_READ_WRITE,
     .report_reasons = RTEMS_FSTAB_NONE,
     .abort_reasons = RTEMS_FSTAB_NONE
    } 
};
/* Read/Write buffers */
static u8 write_buffer[BUFFER_SIZE];
static u8 read_buffer[BUFFER_SIZE];


// 5: Static Function Prototypes
// ----------------------------------------------------------------------------
void *sdCardExample(void *arg);
static bool CheckEmmcFormat(void);
static int SetEmmcFormatMark(void);


// 6: Functions Implementation
// ----------------------------------------------------------------------------

#ifdef FORMAT_CARD

#define NB_OF_PARTITIONS        1

static const msdos_format_request_param_t rqdata = {
/* Optimized for read/write speed */
    .OEMName             = NULL,
    .VolLabel            = NULL,
    .sectors_per_cluster = 64,
    .fat_num             = 0,
    .files_per_root_dir  = 0,
    .media               = 0,
    .quick_format        = true,
    .skip_alignment      = false,
    .sync_device         = false
};

static const rtems_bdpart_format requested_format = {
  .mbr = {
    .type = RTEMS_BDPART_FORMAT_MBR,
    .disk_id = 0xdeadbeef,
    .dos_compatibility = true
  }
};

static const unsigned distribution [NB_OF_PARTITIONS] = {1};

static rtems_bdpart_partition created_partitions [RTEMS_BDPART_PARTITION_NUMBER_HINT];

// Allows to create a partition on the card in case a bare-metal test has overwritten the partition table
static void create_partition(const char *disk,uint32_t partitions)
{
    uint32_t i;
    rtems_status_code status;

    // Initialize partitions types
    for (i = 0; i < partitions; i ++) {
        rtems_bdpart_to_partition_type( RTEMS_BDPART_MBR_FAT_32, created_partitions[i].type );
    }
    // Create 1 partition on sdc0
    status = rtems_bdpart_create(disk, &requested_format, &created_partitions[0], &distribution [0], partitions );
    assert(status == 0);
    //physically write the partition down to media
    status = rtems_bdpart_write(disk, &requested_format, &created_partitions[0], partitions );
    assert(status == 0);
}

#endif


int emmcGpioConfig()
{
    #ifndef D_GPIO_PAD_DRIVE_12mA
    #define D_GPIO_PAD_DRIVE_12mA D_GPIO_PAD_DRIVE_8mA
    #endif

    // If running on MA2x5x and VDDIO_B (for GPIOs 16..21) is
    // powered from 1.8V, we need to set the AON_RETENTION0.23 bit
    #define VDDIO_B_POWERED_FROM_1v8   1  // 0 - 3.3V;  1 - 1.8V

    #if VDDIO_B_POWERED_FROM_1v8 && !defined(MA2100)
    SET_REG_BITS_MASK(AON_RETENTION0_ADR, 1 << 23);
    #endif

    const u32 GPIO_PAD_OPTS_OUT_CLK = 0
            | D_GPIO_PAD_DRIVE_12mA  // configurable: 2mA, 4mA, 8mA, 12mA
          //| D_GPIO_PAD_SLEW_FAST   // configurable
            ;
    const u32 GPIO_PAD_OPTS_OUT_CMD_DAT = 0
            | D_GPIO_PAD_DRIVE_12mA  // configurable: 2mA, 4mA, 8mA, 12mA
          //| D_GPIO_PAD_SLEW_FAST   // configurable
            ;
    const u32 GPIO_PAD_OPTS_IN_OUT = 0
            | GPIO_PAD_OPTS_OUT_CMD_DAT
            | D_GPIO_PAD_RECEIVER_ON
          //| D_GPIO_PAD_SCHMITT_ON  // configurable
            ;
    const u32 CLK_MODE_OPTS = 0
          //| D_GPIO_DATA_INV_ON     // configurable
            ;

    // Configure GPIOs accordingly
    // If operating in 4 bit-mode, DO NOT configure DAT[7:4] in SDIO mode!
#define EMMC
#ifdef EMMC
    DrvGpioModeRange(63, 72, D_GPIO_MODE_2);
    DrvGpioSetMode(63, D_GPIO_MODE_2 | CLK_MODE_OPTS); // clk
    DrvGpioPadSet(63, GPIO_PAD_OPTS_OUT_CLK); // sd_clk

    DrvGpioPadSet(64, GPIO_PAD_OPTS_IN_OUT);  // sd_cmd    -in-out
    DrvGpioPadSet(65, GPIO_PAD_OPTS_IN_OUT);  // sd_dat_0  -in-out
    DrvGpioPadSet(66, GPIO_PAD_OPTS_IN_OUT);  // sd_dat_1 - in-out
    DrvGpioPadSet(67, GPIO_PAD_OPTS_IN_OUT);  // sd_dat_2  -in-out
    DrvGpioPadSet(68, GPIO_PAD_OPTS_IN_OUT);  // sd_dat_3  -in-out
    DrvGpioPadSet(69, GPIO_PAD_OPTS_IN_OUT);  // sd_dat_4  -in-out
    DrvGpioPadSet(70, GPIO_PAD_OPTS_IN_OUT);  // sd_dat_5  -in-out
    DrvGpioPadSet(71, GPIO_PAD_OPTS_IN_OUT);  // sd_dat_6  -in-out
    DrvGpioPadSet(72, GPIO_PAD_OPTS_IN_OUT);  // sd_dat_8  -in-out
#else
    DrvGpioModeRange(46, 51, D_GPIO_MODE_1);
    DrvGpioSetMode(46, D_GPIO_MODE_1 | CLK_MODE_OPTS); // clk
    DrvGpioPadSet(46, GPIO_PAD_OPTS_OUT_CLK); // sd_clk

    DrvGpioPadSet(47, GPIO_PAD_OPTS_IN_OUT);  // sd_cmd    -in-out
    DrvGpioPadSet(48, GPIO_PAD_OPTS_IN_OUT);  // sd_dat_0  -in-out
    DrvGpioPadSet(49, GPIO_PAD_OPTS_IN_OUT);  // sd_dat_1 -in-out
    DrvGpioPadSet(50, GPIO_PAD_OPTS_IN_OUT);  // sd_dat_2  -in-out
    DrvGpioPadSet(51, GPIO_PAD_OPTS_IN_OUT);  // sd_dat_3  -in-out
//    DrvGpioPadSet(69, GPIO_PAD_OPTS_IN_OUT);  // sd_dat_4  -in-out
//    DrvGpioPadSet(70, GPIO_PAD_OPTS_IN_OUT);  // sd_dat_5  -in-out
//    DrvGpioPadSet(71, GPIO_PAD_OPTS_IN_OUT);  // sd_dat_6  -in-out
//    DrvGpioPadSet(72, GPIO_PAD_OPTS_IN_OUT);  // sd_dat_8  -in-out
#endif

#if 0 //modify by lhf 20171110
    DrvGpioModeRange(16,21,D_GPIO_MODE_3);
    DrvGpioSetMode(17, D_GPIO_MODE_3 | CLK_MODE_OPTS);
    DrvGpioPadSet(16, GPIO_PAD_OPTS_IN_OUT);  // sd_dat_3  -in-out
    DrvGpioPadSet(17, GPIO_PAD_OPTS_OUT_CLK); // sd_clk
    DrvGpioPadSet(18, GPIO_PAD_OPTS_IN_OUT);  // sd_cmd    -in-out
    DrvGpioPadSet(19, GPIO_PAD_OPTS_IN_OUT);  // sd_dat_0  -in-out
    DrvGpioPadSet(20, GPIO_PAD_OPTS_IN_OUT);  // sd_dat_1  -in-out
    DrvGpioPadSet(21, GPIO_PAD_OPTS_IN_OUT);  // sd_dat_2  -in-out
#endif
    return 0;
}


void init_emmc(void)
{
    int status = 0;
    rtems_status_code sc;

    swcLeonSetPIL(0);
	// Provide info to the sd card driver
    osDrvSdioEntries_t info = { 1,							// Number of slots
                                DEFAULT_SDIO_INT_PRIORITY,	// Interrupt priority
                               {{SDIO_SLOT_USED,			// Card slot to be used
                                SDIO_DEVNAME_USED}}};		// Device name  
    // Board configuration used
    const tyOsBoard0182Configuration brdConfig[] =
    {
        {
            .type = MV182_END,
            .value = 0
        },
    };
	UNUSED(brdConfig);
    printf ("\nRTEMS POSIX Started\n");  
	
    emmcGpioConfig();

    // Init the Sdio Driver
    printf("Sdio driver initialising \n");
    status = OsDrvSdioInit(&info);
    printf("OsDrvSdioInit sc %s \n", rtems_status_text(status));

	sc = rtems_bdpart_register_from_disk(SDIO_DEVNAME_USED);	
    printf("rtems_bdpart_register_from_disk sc %s \n", rtems_status_text(sc));
	
	// Mount the dosfs
	sc = rtems_fsmount( fs_table, sizeof(fs_table)/sizeof(fs_table[0]), NULL); 
	printf("Mounting File System %s \n", rtems_status_text(sc));
	
	if (!CheckEmmcFormat()) {
		// Create partition before formatting it
        create_partition(SDIO_DEVNAME_USED, NB_OF_PARTITIONS);
		printf("/dev/sdc0 create_partition\n");
		
		sc = rtems_bdpart_register_from_disk(SDIO_DEVNAME_USED);	
    	printf("rtems_bdpart_register_from_disk sc %s \n", rtems_status_text(sc));

		status = msdos_format("/dev/sdc01", &rqdata );
        printf("Formatted the SD card (status = %d) \n", status);
        assert(status == 0);

		// Mount the dosfs
		sc = rtems_fsmount( fs_table, sizeof(fs_table)/sizeof(fs_table[0]), NULL); 
		printf("Mounting File System %s \n", rtems_status_text(sc));

		SetEmmcFormatMark();
	}
}


void setup_emmc(int format)
{
    int status;
    rtems_status_code sc;

    swcLeonSetPIL(0);
	// Provide info to the sd card driver
    osDrvSdioEntries_t info = { 1,  // Number of slots
                                DEFAULT_SDIO_INT_PRIORITY, // Interrupt priority
                               {{SDIO_SLOT_USED, // Card slot to be used
                                SDIO_DEVNAME_USED}}}; // Device name  
    // Board configuration used
    const tyOsBoard0182Configuration brdConfig[] =
    {
        {
            .type = MV182_END,
            .value = 0
        },
    };
	UNUSED(brdConfig);
    printf ("\nRTEMS POSIX Started\n");  
    //Init Clocks 
    //initClocksAndMemory();
    //OsDrvTimerInit();

#ifndef MMC_BUILD // passed from Makefile
    // Init board driver
#else
    emmcGpioConfig();
#endif
    // Init the Sdio Driver
    printf("\nSdio driver initialising \n");
    status = OsDrvSdioInit(&info);
    printf("\nOsDrvSdioInit sc %s \n", rtems_status_text(status));
	
#ifdef FORMAT_CARD
    if (format != 0)
    {
        // Create partition before formatting it
        create_partition(SDIO_DEVNAME_USED, NB_OF_PARTITIONS);
    }
#endif

    sc = rtems_bdpart_register_from_disk(SDIO_DEVNAME_USED);	
    printf("\nrtems_bdpart_register_from_disk sc %s \n", rtems_status_text(sc));
	
#ifdef FORMAT_CARD
    if (format != 0)
    {
        status = msdos_format("/dev/sdc01", &rqdata );
        printf("\nFormatted the SD card (status = %d) \n", status);
        assert(status == 0);
    }
#endif
    // Mount the dosfs
    sc = rtems_fsmount( fs_table, sizeof(fs_table)/sizeof(fs_table[0]), NULL); 
    printf("\nMounting File System %s \n", rtems_status_text(sc));

    //sdCardExample2(NULL);
}


static void fillInBuffers(void)
{
	int i = 0;

	for (i = 0; i < BUFFER_SIZE; i++)
	{
		// This buffer should contain data
		write_buffer[i] = i;
		// This buffer should be empty
		read_buffer[i] = 0;
	}	
}

static void printSpeed(u64 tickCount, u32 sizeBytes)
{
    double time_ms = DrvTimerTicksToMs(tickCount);
    double time_s = time_ms / 1000.0;
    double sizeMB  = (double)sizeBytes / 1000.0 / 1000.0;
    double sizeMiB = (double)sizeBytes / 1024.0 / 1024.0;
    printf("\tSize: %7.3f\n\ttimeMS %9.3f\n\tspeed%8.3f\n\tspeed %8.3f,",
            sizeMiB, time_ms, sizeMB/time_s, sizeMiB/time_s);
}

void *sdCardExample2(void *arg)
{
    UNUSED(arg);

	int fd = 0;
	rtems_status_code sc;	
    const char file[] = "/mnt/sdcard/myfile";

    u64 ticks1,ticks2,ticks3,ticks4;
	// Init buffers
	fillInBuffers();		
		
    #if 1
	// Try to create the file if does not exist
	printf("\nCreating file %s\n", file);
    fd = creat(file, S_IRWXU | S_IRWXG | S_IRWXO ) ;
	assert(fd);	
	close(fd);

    fd = open(file, O_RDWR);
    assert(fd);

	printf("\nWriting %d bytes to file\n", BUFFER_SIZE);
	OsDrvTimerGetSystemTicks64(&ticks1);
	sc = write(fd, write_buffer, BUFFER_SIZE);
	OsDrvTimerGetSystemTicks64(&ticks2);
	printSpeed(ticks2 - ticks1, BUFFER_SIZE);
	assert(sc);
		

	printf("\nPerform fsync\n");
    OsDrvTimerGetSystemTicks64(&ticks3);
	sc = fsync(fd);
    OsDrvTimerGetSystemTicks64(&ticks4);
    printSpeed(ticks2 - ticks1 + ticks4 - ticks3, BUFFER_SIZE);
	assert((sc == 0));
	
	printf("\nClosing file\n");
	sc = close(fd);
	assert((sc == 0));
	#endif
	// Validate written data
	printf("\nOpening file %s \n", file);
	fd = open("/mnt/sdcard/myfile", O_RDWR);
	assert(fd);
	
	printf("\nRead %d characters\n", BUFFER_SIZE);
    OsDrvTimerGetSystemTicks64(&ticks1);
	sc = read(fd, read_buffer, BUFFER_SIZE);
    OsDrvTimerGetSystemTicks64(&ticks2);
    printSpeed(ticks2 - ticks1, BUFFER_SIZE);
	assert(sc);
	
	sc = close(fd);
	// Check Values of read and written data
	printf("\nVerifying data...\n");

    // for (int i = 0; i < 64; i++)
    // {
    //     printf("%d %x %x\n", i, read_buffer[i], write_buffer[i]);
    // }
    printf("test end\n");
    #if 0		
    // Unmount file system
    sc  = unmount("/mnt/sdcard");

    if (sc == 0)
        printf("\nCard successfully unmounted\n\n");
    else
        printf("\nError unmounting card %s\n", rtems_status_text(sc));
    assert(sc == 0);
    #endif
	// assert(memcmp(read_buffer, write_buffer, BUFFER_SIZE) == 0);
	
    return NULL; // just so the compiler thinks we returned something
}
#include <swcCrc.h>
#include <sys/stat.h>
#include <unistd.h>
#include <errno.h>

void checkFileCRC(char* filepath)
{
    struct stat s;
    if (stat(filepath, &s) == 0)
    {
        int filesize = s.st_size;

        int fd = open(filepath, O_RDONLY);
        int sc = read(fd, read_buffer, filesize);
        close(fd);
        u32 crc2 = swcCalcCrc32(read_buffer,filesize,le_pointer);
        printf("%s size %d crc %x\n", filepath, filesize, crc2);
    }
    else
    {
         printf("%s stat error %d\n", filepath, errno);
    }
}


int listFilesInRoot()
{
    DIR* dir_pointer = NULL;
    struct dirent *pDirEntry = NULL;
    struct stat s;
    int found = 0;
    char file_path[50];
    long long tmp_size;

    dir_pointer = opendir("/mnt/sdcard");

    printf("list /mnt/sdcard \n");

    if(!dir_pointer)
    {
        printf("opendir failed!\n");
        return -1;
    }

    while( (pDirEntry = readdir(dir_pointer)) != NULL )
    {
        printf(" - %s\n", pDirEntry->d_name);
    }
	
	return EmmcCheck();
}

void listFilesInBlobs()
{
    DIR* dir_pointer = NULL;
    struct dirent *pDirEntry = NULL;
    struct stat s;
    int found = 0;
    char file_path[50];
    long long tmp_size;

    dir_pointer = opendir("/mnt/sdcard/blobs");

    printf("list /mnt/sdcard/blobs \n");

    if(!dir_pointer)
    {
        printf("opendir failed!\n");
        return ;
    }

    while( (pDirEntry = readdir(dir_pointer)) != NULL )
    {
        printf(" - %s\n", pDirEntry->d_name);
    }
}

static bool CheckEmmcFormat(void)
{
	if (0 != access("/mnt/sdcard/EmmcFormatMark", F_OK)) {
		printf("EMMC Don't Format!\n");
		return false;
	}
	return true;
}

static int SetEmmcFormatMark(void)
{
	FILE *pFile = fopen("/mnt/sdcard/EmmcFormatMark","w+");
	if (!pFile) {
		printf("fopen /mnt/sdcard/EmmcFormatMark failed\n");
		return -1;
	}
	fwrite("EmmcFormatMark=1",sizeof(char), 20, pFile);
    fclose(pFile);
	printf("EMMC Format Success.\n");
	return 0;
}

int saveFileToEMMC(char *input_buf,int file_size,char *outFile)
{
    FILE *fp;
    char file_name[30] = {0};
    sprintf(file_name,"%s%s","/mnt/sdcard/",outFile);
    printf("save file name %s.\n",file_name);
    if ((fp = fopen(file_name,"w+")) == NULL)
    {
        printf("open file failed!-->%s\n",file_name);
        return -1;
    }

    printf("file_size:%d\n",file_size);
    if (fwrite(input_buf,file_size,1,fp)!=1)
    {
        printf("write file failed!\n");
        return -1;
    }
    fclose(fp);
    return 0;
}
int getFileFromEMMC(char *output_buf,int size,char *outFile)
{
    FILE *fp;
    char file_name[30] = {0};
    sprintf(file_name,"%s%s","/mnt/sdcard/",outFile);
    printf("save file name %s.\n",file_name);
    if ((fp = fopen(file_name,"r")) == NULL)
    {
        printf("open file failed!-->%s\n",file_name);
        return -1;
    }

    printf("file_size:%d\n",size);
    if (fread(output_buf,1,size,fp))
    {
        printf("read  file succed!\n");
    }
    fclose(fp);
    return 0;
}
int EmmcCheck(void)
{	
	char testStr[100];
	
	memset(testStr, 0, sizeof(testStr));
	memcpy(testStr, "Check whether or not the emmc is read and write properly!", 100);
	int length = strlen(testStr);
	
	FILE *pFile = fopen("/mnt/sdcard/EmmcCheck.txt","w+");
	if (!pFile)
	{
		return -1;
	}
	
	if (length != fwrite(testStr, sizeof(char), length, pFile)) 
	{
		fclose(pFile);
		return -1;
	}
	fclose(pFile);
	pFile = NULL;
		
	u32 crc2Str = swcCalcCrc32(testStr, length, le_pointer);
	printf("crc2Str:%x\n", crc2Str);
	
	struct stat s;
	u32 crc2file = 0;
    if (stat("/mnt/sdcard/EmmcCheck.txt", &s) == 0) 
	{
        int filesize = s.st_size;
		
        pFile = fopen("/mnt/sdcard/EmmcCheck.txt", "r");
		if (pFile)
		{
			memset(testStr, 0, sizeof(testStr));
        	fread(testStr, sizeof(char), filesize, pFile);
			fclose(pFile);
			pFile = NULL;
			crc2file = swcCalcCrc32(testStr, filesize, le_pointer);
			printf("crc2file:%x\n", crc2file);
		}
    }
	
	if (crc2Str == crc2file)
	{
		printf("EMMC File crc2 ok\n");
		return 0;
	}
	printf("EMMC File crc2 error\n");
	
	return -1;
}

void format_emmc(void)
{
    int status = 0;
    rtems_status_code sc;

    swcLeonSetPIL(0);
	/*provide info to the sd card driver*/
    osDrvSdioEntries_t info = { 1,							// Number of slots
                                DEFAULT_SDIO_INT_PRIORITY,	// Interrupt priority
                               {{SDIO_SLOT_USED,			// Card slot to be used
                                SDIO_DEVNAME_USED}}};		// Device name  
    /*board configuration used*/
    const tyOsBoard0182Configuration brdConfig[] =
    {
        {
            .type = MV182_END,
            .value = 0
        },
    };
	UNUSED(brdConfig);
	
    printf ("EMMC GPIO CONFIG..\n");
    emmcGpioConfig();
	
    /*init the Sdio Driver*/
    printf("Sdio driver initialising \n");
    status = OsDrvSdioInit(&info);
    printf("OsDrvSdioInit sc %s \n", rtems_status_text(status));
	
	sc = rtems_bdpart_register_from_disk(SDIO_DEVNAME_USED);
    printf("rtems_bdpart_register_from_disk sc %s \n", rtems_status_text(sc));
	
	/*create partition before formatting it*/
	create_partition(SDIO_DEVNAME_USED, NB_OF_PARTITIONS);
	printf("/dev/sdc0 create_partition\n");		

	status = msdos_format("/dev/sdc01", &rqdata );
	assert(status == 0);
	printf("Formatted the SD card (status = %d) \n", status);
	
	/*mount the dosfs*/
	sc = rtems_fsmount( fs_table, sizeof(fs_table)/sizeof(fs_table[0]), NULL); 
	printf("Mounting File System %s \n", rtems_status_text(sc));
		
	SetEmmcFormatMark();
}

