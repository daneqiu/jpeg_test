#ifndef _EMMC_H
#define _EMMC_H

#define EMMC_PATH "/dev/sdc01"
void setup_emmc(int format);
void *sdCardExample2(void *arg);
int listFilesInRoot();
void listFilesInBlobs();
void init_emmc(void);
int saveFileToEMMC(char *input_buf,int file_size,char *outFile);
int getFileFromEMMC(char *output_buf,int size,char *outFile);
int EmmcCheck(void);
void format_emmc(void);

#endif

